import bloc1_base64_binary_IO as bIO

BASE64_SYMBOLS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                  'o', 'p', 'q', 'r', 's', 'thttps://www.google.com/search?ei=uksLXe_HEeOp8gKxpJSICg&q=python+decimal+vers+binaire&oq=python+decimal+vers+binaire&gs_l=psy-ab.3..0i22i30.2754.9944..10111...0.0..0.82.1216.21......0....1..gws-wiz.......0i71j33i22i29i30j0i8i13i30.oezmi-3ZaTA', 'u', 'v',
                  'w', 'x', 'y', 'z', '0', '1', '2', '3',
                  '4', '5', '6', '7', '8', '9', '+', '/']

def to_base64(triplet):
    '''
	convertit le triplet d'octets en une chaîne de quatre symboles
	
	:param triplet: (tuple ou list) une séquence d'octets
	:return: (str) la chaîne de symboles de la base 64 représentant le triplet d'octets
	:CU: 1 <= len(triplet) <= 3 et les entiers de triplet tous compris entre 0 et 255
	:Exemple:
	
	>>> to_base64((18, 184, 156))
	'Eric'
	>>> to_base64((18, 184))
	'Erg='
	>>> to_base64((18,))
	'Eg=='
	'''
    if len(triplet)==3:
        a=triplet[0]>>2
        b=triplet[1]>>4|(triplet[0]&0b11)<<4
        c=triplet[2]>>6|(triplet[1]&0b1111)<<2
        d=triplet[2]&0b111111
        return BASE64_SYMBOLS[a]+BASE64_SYMBOLS[b]+BASE64_SYMBOLS[c]+BASE64_SYMBOLS[d]
    elif len(triplet)==2:
        a=triplet[0]>>2
        b=triplet[1]>>4|(triplet[0]&0b11)<<4
        c=(triplet[1]&0b1111)<<2
        return BASE64_SYMBOLS[a]+BASE64_SYMBOLS[b]+BASE64_SYMBOLS[c]
    else :
        a=triplet[0]>>2
        b=(triplet[0]&0b11)<<4
        return BASE64_SYMBOLS[a]+BASE64_SYMBOLS[b]
    
def from_base64(b64_string):
    '''
    convertit une chaîne de quatre symboles en un tuple (le plus souvent triplet) d'octets
    
    :param b64_string: (str) une chaîne de symboles de la base 64
    :return: (tuple) un tuple d'octets dont b64_string est la représentation en base 64
    :CU: len(b64_string) == 4 et les caractères de b64_string sont dans la table ou le symbole =
    :Exemple:
    
    >>> from_base64('Eric')
    (18, 184, 156)
    >>> from_base64('Erg=')
    (18, 184)
    >>> from_base64('Eg==')
    (18,)
    '''
    s0=BASE64_SYMBOLS.index(b64_string[0])
    s1=BASE64_SYMBOLS.index(b64_string[1])
    s2=BASE64_SYMBOLS.index(b64_string[2])
    s3=BASE64_SYMBOLS.index(b64_string[3])
    
    a=s0<<2|(s1&0b110000)>>4
    b=(s1&0b1111)<<4|(s2&0b111100)>>2
    c=(s2&0b11)<<6|(s3&0b111111)
    return (a,b,c)

def base64_encode(source):
    '''
 	Encode a file in base64 and outputs the result on standard output.

 	:param source: (str) the source filename
 	:return: None
 	:side effect: print on the standard output the base64 encoded version of the content 
                   of source file
 	'''

    reader = bIO.Reader(source)
    cpt=0
    while True:
        contenu=reader.get_bytes(3)
        if len(contenu)!=0:
            cpt+=1
            print(to_base64(contenu),end="")
            if cpt==19:
                print()
                cpt=0
        else:
            break

 