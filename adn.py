from random import randint
def estADN(chaine):
    """
    teste si une chaine ADN est valide
    :param chaine:(str) une chaine de caractère sensée etre une chaine ADN
    :return: (bool)
    :CU: aucune
    
    >>> estADN("ACGT")
    True
    >>> estADN("ASSD")
    False
    >>> estADN("ACgT")
    False
    """
    for lettre in chaine:
       if lettre not in "ACGT":
           return False
    return True

def genereADN(taille):
    """
    génère une chaine ADN "possible de n caractères"
    :param taille:(int) la taille
    :return: (str) une chaine ADN du bon nombre de caractères
    :CU: aucune
    
    >>> genereADN(10)
    'CCAATTACTA'
    >>> genereADN(15)
    'GCTGCCACAGCGATA'
    """
    ADN=""
    lettres="ACGT"
    for i in range(taille):
        ADN+=lettres[randint(0,3)]
    return ADN

def basecomplementaire(char,type_de_chaine):
    """
    génère la chaine complémentaire ADN ou ARN
    :param char:(str) la base
    :return: (str) la base complémentaire
    :CU: aucune
    
    >>> basecomplementaire("A",'ADN')
    'T'
    >>> basecomplementaire("A",'ARN')
    'U'
    """
    retour=""
    if char=="T":
        retour+="A"
    elif char=="G":
        retour+="C"
    elif char=="C":
        retour+="G"
    elif type_de_chaine=="ADN":
        retour+="T"
    else:
        retour+="U"
    return retour

def transcrit(chaine,départ,fin):
    """
    renvoie l'ARN construit à partir de la sous-séquence d'ADN comprise entre les deux positions
    passées en paramètre,
    :param chaine:(str) une chaine ADN
    :return: (str) une chaine ARN extraite
    :CU: aucune
    
    >>> transcrit('TTCTTCTTCGTACTTTGTGCTGGCCTCCACACGATAATCC', 4, 23)
    'AAGAAGCAUGAAACACGACC'
    """
    retour=""
    for char in chaine[départ-1:fin]:
        retour+=basecomplementaire(char,"ARN")
    return retour

def traduit(chaine):
    """construit la séquence protéique
    obtenue par la traduction de la séquence ARN passée en paramètre
    :param chaine:(str) une chaine ADN
    :return: (str) 
    :CU: aucune
    
    >>> traduit('AUGCGAAGCCGAAAGAACACCGGCUAA')
    'MRSRKNTG*'
    """
    retourne=""
    codons=[chaine[3*i:3*i+3] for i in range(len(chaine)//3)]
    for cod in codons:
        if cod in ['UUU', 'UUC']:
            retourne+="F"
        elif cod in ['UUA', 'UUG', 'CUU', 'CUC', 'CUA', 'CUG']:
            retourne+="L"
        elif cod in ['AUU', 'AUC', 'AUA']:
            retourne+="I"
        elif cod in ['AUG']:
            retourne+="M"
        elif cod in ['GUU', 'GUC', 'GUA', 'GUG']:
            retourne+="V"
        elif cod in ['UCU', 'UCC', 'UCA', 'UCG', 'AGU', 'AGC']:
            retourne+="S"
        elif cod in ['CCU', 'CCC', 'CCA', 'CCG']:
            retourne+="P"
        elif cod in ['ACU', 'ACC', 'ACA', 'ACG']:
            retourne+="T"
        elif cod in ['GCU', 'GCC', 'GCA', 'GCG']:
            retourne+="A"
        elif cod in ['UAU', 'UAC']:
            retourne+="Y"
        elif cod in ['UAA', 'UAG', 'UGA']:
            retourne+="*"
        elif cod in ['CAU', 'CAC']:
            retourne+="H"
        elif cod in ['CAA', 'CAG']:
            retourne+="Q"
        elif cod in ['AAU', 'AAC']:
            retourne+="N"
        elif cod in ['AAA', 'AAG']:
            retourne+="K"
        elif cod in ['GAU', 'GAC']:
            retourne+="D"
        elif cod in ['GAA', 'GAG']:
            retourne+="E"
        elif cod in ['UGU', 'UGC']:
            retourne+="C"
        elif cod in ['UGG']:
            retourne+="W"
        elif cod in ['CGU', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG']:
            retourne+="R"
        elif cod in ['GGU', 'GGC', 'GGA', 'GGG']:
            retourne+="G"
    return retourne

def replique(chaine):
    """

    construit la séquence ADN complémentaire et inversée de celle passée en paramètre
    :param chaine:(str) une chaine ADN
    :return: (str) 
    :CU: aucune
    
    >>> replique('ACTG')
    'CAGT'
    """
    retourne=""
    for char in chaine:
        retourne+=basecomplementaire(char,"ADN")
    return retourne[-1::-1]

print("menu")
print ("1 teste si une chaine est possible")
print ("2 generation d'un chaine ADN")
print ("3 chaine ARN associée de la partie de la chaine")
print ("4 séquence protéique")
print ("5 séquence complémentaire inverse")
while 1:
    choix=input ("votre choix: ")
    if choix=='1':
        chaine=input ("donner la chaine à vérifier : ")
        if estADN(chaine):
            print (f"la chaine {chaine} est bien une chaine ADN")
        else:
            print (f"la chaine {chaine} n'est pas une chaine ADN")
    if choix=='2':
        taille=input ("donner le nombre de bases: ")
        print (f"voici la sequence ADN  {genereADN(int(taille))}")
    if choix=='3':
        chaine=input ("donner la chaine à utiliser : ")
        deb=input ("donner l'indice de début : ")
        fin=input ("donner l'indice de fin : ")
        if estADN(chaine):
            print(f"la chaine ARN extraite est {transcrit(chaine,int(deb),int(fin))}")
        else:
            print (f"la chaine {chaine} n'est pas une chaine ADN")
    if choix=='4':
        chaine=input ("donner la chaine à utiliser : ")
        print(f"la chaine protéinique est {traduit(chaine)}")
    if choix=='5':
        chaine=input ("donner la chaine à utiliser : ")
        if estADN(chaine):
            print(f"la chaine repliquée est {replique(chaine)}")
        else:
            print (f"la chaine {chaine} n'est pas une chaine ADN")