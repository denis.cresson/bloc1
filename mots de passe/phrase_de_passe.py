import doctest
import random
from liste_7776_mots import LISTE_MOTS

des_chaines=all(isinstance(mot,str) for mot in LISTE_MOTS)
lettres_uniquement=all(lettre.upper() in "AZERETYUIOPQSDFGHJKLMWXCVBN" for mot in LISTE_MOTS for lettre in mot )
uniques=sum(mot1==mot2 for mot1 in LISTE_MOTS for mot2 in LISTE_MOTS)==7776
uniques2=all(LISTE_MOTS.count(mot)==1 for mot in LISTE_MOTS)
minimum=min(len(mot) for mot in LISTE_MOTS)
maximum=max(len(mot) for mot in LISTE_MOTS)


def affiche_un_mot(n):
    """
    affiche la ligne du mot de cette liste d'indice n
    :param n:(int) indice du mot
    :return:(noneType)
    :CU: None
    :effets de bord: aucun
    """
    mot=LISTE_MOTS[n]
    print(f"{n} : {mot}")
    
def mot(n):
    """
    affiche le mot de cette liste d'indice n, le dernier et celui d'indice 2094
    :param n:(int) indice du mot
    :return:(noneType)
    :CU: None>>> genere_n_sequences_alea(2)
'5311514166'
>>> genere_n_sequences_alea(3)
'111116666624521'
    :effets de bord: affiche les mots
    """
    affiche_un_mot(n)
    affiche_un_mot(7775)
    affiche_un_mot(2094)

def en_nombre(chaine):
    """
    converti la chaine codant le lancer de 6 dés en nombre
    :param chaine:(list)
    :return :(int) le nombre entier correspondant à l'ecriture en base 6
    :CU: necessite une chaine valide
    
    >>> en_nombre('11111')
    0
    >>> en_nombre('66666')
    7775
    >>> en_nombre('24521')
    2094

    """
    retour=0
    for char in chaine:
        retour=retour*6+(int(char)-1)
    return retour

def donne_mot(chaine):
    """
    donne le mot correspondant à la chaine codant le lancer de 6 dés en nombre
    :param chaine:(list)
    :return :(str) 
    :CU: necessite une chaine valide
    
    >>> donne_mot('11111')
    'montes'
    >>> donne_mot('66666')
    'tour'
    >>> donne_mot('24521')
    'morigener'


    """
    return LISTE_MOTS[en_nombre(chaine)]

def genere_n_sequences_aleagit(n):
    """
    génère une chaine de caractères contenant n séquences de 5 charactères (5 lancers)
    :param n:(int) le nombre de séquences
    :return:(str)
    :CU: None
    
    >>> genere_n_sequences_alea(2)
    '5311514166'
    >>> genere_n_sequences_alea(3)
    '111116666624521'
    """
    retour=""
    for i in range(n):
        for j in range(5):
            retour+="123456"[random.randint(0,5)]
    return retour

def genere_phrase_passe(n):
    """
    génère une phrase passe de n mots
    :param n:(int) le nombre de mots
    :return:(str)
    :CU: None
    
    >>> genere_phrase_passe('111116666624521')
    'montes-tour-morigener'

    """
    retour=""
    chaine=genere_n_sequences_alea(n)
    liste=(chaine[i:i+5] for i in range(len(chaine)//5))
    for motnombre in liste:
        retour+=donne_mot(motnombre)+ '-'
    return retour[:-1]

def perturbe_chaine(chaine):
    """
    retourne une chaine avec des majuscules de manière aléatoire
    :param chaine:(str)
    :return: (str)
    :CU: none
    >>> perturbe_chaine("timoleon")
    'timolEOn'
    >>> perturbe_chaine("timoleon")
    'tIMOlEON'

    """
    retourne=""
    for char in chaine:
        if random.randint(0,1)==0:
            retourne+=char.upper()
        else:
            retourne+=char
    return retourne

def perturbe_chaine2(chaine):
    """
    retourne une chaine avec des majuscules de manière aléatoire et sinon une modif aleatoire en fonction du dictionnaire
    :param chaine:(str)
    :return: (str)
    :CU: none
    >>> perturbe_chaine2("timoleon")
    'TiMol30N'
    >>> perturbe_chaine2("timoleon")
    'tiMol3on'
    >>> perturbe_chaine2("timoleon")
    'T1m0LeON'
    """
    EQUIVALENTS = {
        'a' : '@',
        'b' : '8',
        'e' : '3',
        'i' : '1',
        'o' : '0'
    }
    retourne=""
    for char in chaine:
        if random.randint(0,1)==0:
            retourne+=char.upper()
        else:
            if char in EQUIVALENTS:
                if random.randint(0,1)==0:
                    retourne+=EQUIVALENTS[char]
                else:
                    retourne+=char
            else:
                retourne+=char
    return retourne

def genere_phrase_passe3(n):
    """
    génère une phrase passe de n mots en utilisant les modification entropiques
    :param n:(int) le nombre de mots
    :return:(str)
    :CU: None
    
    >>> genere_phrase_passe('111116666624521')
    'montes-tour-morigener'

    """
    retour=genere_phrase_passe(n)
    return perturbe_chaine2(retour)

doctest.testmod(verbose=True)