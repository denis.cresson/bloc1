# PROJET

## Choix du jeu de nim et du tic tac toe

## Utilisation
par defaut jeu.py fonctione avec le module nim et la profondeur 3 mais ces paramètres peuvent être modifiés en utilisant les arguments de programme

```
>>> %Run jeu.py
entrer le nom du joueur (" " pour un joueur géré par la machine) : 
entrer le nom du joueur (" " pour un joueur géré par la machine) : 
il reste 20 allumettes
l'ordinateur1 joue :  2
il reste 18 allumettes
l'ordinateur2 joue :  2
il reste 16 allumettes
l'ordinateur1 joue :  3
il reste 13 allumettes
l'ordinateur2 joue :  2
il reste 11 allumettes
l'ordinateur1 joue :  3
il reste 8 allumettes
l'ordinateur2 joue :  2
il reste 6 allumettes
l'ordinateur1 joue :  3
il reste 3 allumettes
l'ordinateur2 joue :  3
il ne reste plus d'allumettes
bravo ordinateur2 vous avez gagné
```

utilisation avec tic_tac_toe une profondeur de 4 et deux fois l'ordinateur
```
>>> %Run jeu.py tic_tac_toe 4
entrer le nom du joueur (" " pour un joueur géré par la machine) : 
entrer le nom du joueur (" " pour un joueur géré par la machine) : 
   a   b   c

1    |   |   
  -----------
2    |   |   
  -----------
3    |   |   
l'ordinateur1 joue :  b2
   a   b   c

1    |   |   
  -----------
2    | x |   
  -----------
3    |   |   
l'ordinateur2 joue :  a1
   a   b   c

1  o |   |   
  -----------
2    | x |   
  -----------
3    |   |   
l'ordinateur1 joue :  b1
   a   b   c

1  o | x |   
  -----------
2    | x |   
  -----------
3    |   |   
l'ordinateur2 joue :  b3
   a   b   c

1  o | x |   
  -----------
2    | x |   
  -----------
3    | o |   
l'ordinateur1 joue :  a2
   a   b   c
https://gitlab-fil.univ-lille.fr/denis.cresson/bloc1/blob/master/projet/activit%C3%A9_eleve.md
1  o | x |   
  -----------
2  x | x |   
  -----------
3    | o |   
l'ordinateur2 joue :  c2
   a   b   c

1  o | x |   
  -----------
2  x | x | o 
  -----------
3    | o |   
l'ordinateur1 joue :  c1
   a   b   c

1  o | x | x 
  -----------il reste une allumette
désolé match nul
2  x | x | o 
  -----------
3    | o |   
l'ordinateur2 joue :  a3
   a   b   c

1  o | x | x 
  -----------
2  x | x | o 
  -----------
3  o | o |   
l'ordinateur1 joue :  c3
   a   b   c

1  o | x | x 
  -----------
2  x | x | o 
  -----------
3  o | o | x 
désolé match nul
>>> https://gitlab-fil.univ-lille.fr/denis.cresson/bloc1/blob/master/projet/activit%C3%A9_eleve.md
```
un cas interessant pour tic_tac_toe et une profondeur de 2, la fonction d'évaluation permet de "perdre" avec sa vision trop courte (et la case centrale non prise)
```
>>> %Run jeu.py tic_tac_toe 2
entrer le nom du joueur (" " pour un joueur géré par la machine) : 
entrer le nom du joueur (" " pour un joueur géré par la machine) : 
   a   b   c

1    |   |   
  -----------
2    |   |   
  -----------
3    |   |   
l'ordinateur1 joue :  a1
   a   b   c

1  x |   |   
  -----------
2    |   |   
  -----------
3    |   |   
l'ordinateur2 joue :  b1
   a   b   c

1  x | o |   
  -----------
2    |   |   
  -----------
3    |   |   
l'ordinateur1 joue :  a2
   a   b   c

1  x | o |   
  -----------
2  x |   |   
  -----------
3    |   |   
l'ordinateur2 joue :  a3
   a   b   c

1  x | o |   
  -----------
2  x |   |   
  -----------
3  o |   |   
l'ordinateur1 joue :  b2
   a   b   c

1  x | o |   
  -----------
2  x | x |   
  -----------
3  o |   |   
l'ordinateur2 joue :  c1
   a   b   c

1  x | o | o 
  -----------
2  x | x |   
  -----------
3  o |   |   
l'ordinateur1 joue :  c2
   a   b   c

1  x | o | o 
  -----------
2  x | x | x 
  -----------
3  o |   |   
bravo ordinateur1 vous avez gagné
```
le choix de c1 parait bizare mais est cohérent car aucun coup ne permet alors de ne pas perdre

## [FICHE ELEVE](https://gitlab-fil.univ-lille.fr/denis.cresson/bloc1/blob/master/projet/activit%C3%A9_eleve.md)
