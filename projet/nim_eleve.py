
def creer_situation_courante():
    """
    retourne le nombre d'allumettes du jeu de nim (entre 5 et 25)
    :return:(int)
    """
    return 15


def afficher(nombre_allumettes):
    """
    affiche le nombre d'allumettes
    :param nombre_allumettes:(int)
    :return:None
    :effet de bord:affichage
    """
    print("à vous")
    
def coups_possibles(nombre_allumettes):
    """
    retourne la liste des coup possibles à jouer
    :param nombre_allumettes:(int)
    :return:(list)
    """
    return [str(nb) for nb in [2,3] if nb<=int(nombre_allumettes)]

def actualiser(a_retirer,nombre_allumettes,joueur_courant):
    """
    retourne le nouveau nombre d'allumettes
    :param a_retirer:(int)
    :param nombre_allumettes:(int)
    :param joueur_courant:(int) inutile ici
    """
    return nombre_allumettes-int(a_retirer)

def verification_fin_jeu(nombre_allumettes,joueur_courant):
    """
    teste si le jeu est fini et retourne 1 si match nul 0 si gagnant joueur1 ou 2 si gagnant joueur 2  -1 sinon
    :param nombre_allumettes:(int)
    :param joueur_courant:(int) 0 ou 1
    :return:(int)
    """
    retour=-1
    if nombre_allumettes==1:
        retour=1
    elif nombre_allumettes==0:
        retour=joueur_courant*2
    return retour

def situations_possibles(situation,joueur):
    """
    retourne la liste des situations possibles pour un joueur à partir d'une situation de départ
    :param situation:(int)
    :param joueur:(str)
    :return:(list)
    """
    cp_poss=coups_possibles(situation)
    return [str(int(situation)-int(coup)) for coup in cp_poss]

