from random import randint

def creer_situation_courante():
    """
    retourne le nombre d'allumettes du jeu de nim (entre 5 et 25)
    :return:(int)
    """
    return randint(5,25)


def afficher(nombre_allumettes):
    """
    affiche le nombre d'allumettes
    :param nombre_allumettes:(int)
    :return:None
    :effet de bord:affichage
    """
    if nombre_allumettes>1:
        print("il reste {} allumettes".format(nombre_allumettes))
    elif nombre_allumettes==1:
        print("il reste une allumette")
    else:
        print("il ne reste plus d'allumettes")
    
def coups_possibles(nombre_allumettes):
    """
    retourne la liste des coup possibles à jouer
    :param nombre_allumettes:(int)
    :return:(list)
    """
    return [str(nb) for nb in [2,3] if nb<=int(nombre_allumettes)]

def actualiser(a_retirer,nombre_allumettes,joueur_courant):
    """
    retourne le nouveau nombre d'allumettes
    :param a_retirer:(int)
    :param nombre_allumettes:(int)
    :param joueur_courant:(int) inutile ici
    """
    return nombre_allumettes-int(a_retirer)

def verification_fin_jeu(nombre_allumettes,joueur_courant):
    """
    teste si le jeu est fini et retourne 1 si match nul 0 si gagnant joueur1 ou 2 si gagnant joueur 2  -1 sinon
    :param nombre_allumettes:(int)
    :param joueur_courant:(int) 0 ou 1
    :return:(int)
    """
    retour=-1
    if nombre_allumettes==1:
        retour=1
    elif nombre_allumettes==0:
        retour=joueur_courant*2
    return retour

def situations_possibles(situation,joueur):
    """
    retourne la liste des situations possibles pour un joueur à partir d'une situation de départ
    :param situation:(int)
    :param joueur:(str)
    :return:(list)
    """
    cp_poss=coups_possibles(situation)
    return [str(int(situation)-int(coup)) for coup in cp_poss]

def evalu(situation,joueur):
    """ retourne une valeur numérique évaluant la "valeur" d'une situation pour un joueur
    :param situation:(int)
    :param joueur:(str)
    :return:(int)
    """
    situation=int(situation)

    if situation==0:
        return 1000
    if situation==1:
        return 0
    if situation%5==0:
            retour=500
    elif situation%5==1:
            retour=0
    elif situation%5==2:
            retour=-500
    elif situation%5==3:
            retour=500
    elif situation%5==4:
            retour=250
    return retour