# ACTIVITE ELEVE

## niveau basique

on donne aux élèves une version simplifiée de jeu.py ([jeu_eleve.py](jeu_eleve.py)) et des modules de jeu ([nim_eleve.py](nim_eleve.py) et [tic_tac_toe_eleve.py](tic_tac_toe_eleve.py))

### Interface
* par defaut nim, modifier pour tic_tac_toe
* afficher programme arguments

### NIM
* modifier le nombre d’allumettes pour que le nombre soit aléatoire et compris entre 5 et 50
    * insertion : from random import randint
    * randint(5,50)
     <img src="fwd/randint.png" width=50%>
* affichage une representation des  allumettes(‘|’)
<img src="fwd/screen_allumettes_texte.png" width=60%>

* affichage representation des allumettes + nombre en toute lettres
<img src="fwd/def_afficherx.png" width=40%>

* gestion du ‘s’ des allumettes

### TIC TAC TOE

* affichage d'une representation de la grille du type

<img src="fwd/grille.png" >

* compléter la fonction diags pour tester si une des diagonales est gagnante
* complément réaliser la fonction précédente avec une seule boucle for
```python
>>> def diags(grille,lettre):
    """
    teste si l'une des diagonales de la grille contient 3 fois lettre
    :param grille:(list)
    :param lettre:(str)
    :return:(bool)
    """
    retour=False
    diag1=[]
    diag2=[]
    for j in range(3):
        diag1.append([grille[j][j])
        diag2.append(grille[2-j][j])
    if diag1.count(lettre)==3 or diag2.count(lettre)==3:
        retour=True
    return retour
```

