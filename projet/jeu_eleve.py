
def nom_joueur():
    """
    retourne le nom d'un joueur
    :return:(str) le nom
    """
    retour=input("entrer le nom du joueur : ")
    return retour



def saisir_coup(nom,situation_courante):
    """
    retourne le coup à jouer, saisi par le joueur si ce n'est pas ordinateur, sinon utilise minmax pour trouver le meilleur coup
    :param nom:(str)
    :return:(str)
    """

    return input("que voulez-vous jouer {} : ".format(nom))

    

def gestion_jeu():
    """
    lance la fonction de jeu
    :effet de bord: joue la partie
    """
    joueur=[nom_joueur(),nom_joueur()]
    joueur_courant=0
    situation_courante=game.creer_situation_courante()
    game.afficher(situation_courante)
    jeu=-1
    while jeu==-1:
        coups_possibles=game.coups_possibles(situation_courante)
        coup=saisir_coup(joueur[joueur_courant],situation_courante)
        if coups_possibles!=[] :
            while coup not in coups_possibles:
                coup=saisir_coup(joueur[joueur_courant],situation_courante)
            situation_courante=game.actualiser(coup,situation_courante,joueur_courant)
            game.afficher(situation_courante)
        jeu=game.verification_fin_jeu(situation_courante,joueur_courant)
        joueur_courant=(joueur_courant+1)%2
    phrases=['bravo {} vous avez gagné'.format(joueur[0]),'désolé match nul','bravo {} vous avez gagné'.format(joueur[1])]
    print(phrases[jeu])
                
            
          
#si ce n'est pas un module chargé, importe le module de jeu correspondant à l'argument comme "game" et lance la fonction de jeu 
if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        module = 'nim'
    else:
        module = sys.argv[1]
    game = __import__(module)
    gestion_jeu()