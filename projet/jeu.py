from random import randint
import sys

def nom_joueur():
    """
    retourne le nom d'un joueur
    :return:(str) le nom ou ordinateur
    """
    retour=input("entrer le nom du joueur (\" \" pour un joueur géré par la machine) : ")
    if retour=="":
        retour="ordinateur"
    return retour

coeff={'adversaire':-1,"ordinateur":1}
if len(sys.argv) == 3:
    profondeur=int(sys.argv[2])
else:
    profondeur=3
    
def min_max(situation,profond, joueur):
    """
    implementation de l'algo min max, retourne un tuple contenant le coup a jouer et la valeur associée à celui ci
    :param situation:(dépend du jeu)
    :param profond:(int) la profondeur de l'algorithme
    :param joueur:(str) "adversaire...." ou "ordinateur..." 
    :return:(tuple)
    """
    
    if game.coups_possibles(situation)==[] or profond==0:
        retourner= (game.evalu(situation, joueur) * coeff[joueur[:10]],"")
    else:
        cp_poss=game.coups_possibles(situation)
        situSuivantes = game.situations_possibles(situation,joueur)
        #print(situSuivantes)
        if joueur[:10] == "ordinateur" :
            joueur=joueur.replace("ordinateur","adversaire")
            liste=[min_max(suivante, profond-1, joueur)[0] for suivante in situSuivantes ]
            valeur = max( liste )
            #print(situation, profond-1, joueur,valeur,cp_poss[liste.index(valeur)])
        else: # joueur = `ADVERSAIRE`
            joueur=joueur.replace("adversaire","ordinateur")
            liste=[ min_max(suivante, profond -1, joueur)[0] for suivante in situSuivantes ]
            valeur = min( liste )
            #print(situation, profond-1, joueur,valeur,cp_poss[liste.index(valeur)])
        coup=cp_poss[liste.index(valeur)]
        retourner= (valeur,coup)
    return retourner



def saisir_coup(nom,situation_courante):
    """
    retourne le coup à jouer, saisi par le joueur si ce n'est pas ordinateur, sinon utilise minmax pour trouver le meilleur coup
    :param nom:(str)
    :return:(str)
    """
    if nom[:10]!="ordinateur":
        return input("que voulez-vous jouer {} : ".format(nom))
    else:
        coup=min_max(situation_courante,profondeur,nom)[1]
        print("l'{} joue : ".format(nom),coup)
        return coup
        
        
    

def gestion_jeu():
    """
    lance la fonction de jeu
    :effet de bord: joue la partie
    """
    joueur=[nom_joueur(),nom_joueur()]
    if "ordinateur" not in joueur:
        joueur_courant=randint(0,1)
    else:
        if joueur[0]==joueur[1]=="ordinateur":
            joueur=["ordinateur1","ordinateur2"]
        if joueur[1]=="ordinateur":
            joueur=[joueur[1],joueur[0]]
        joueur_courant=0
    situation_courante=game.creer_situation_courante()
    game.afficher(situation_courante)
    jeu=-1
    while jeu==-1:
        coups_possibles=game.coups_possibles(situation_courante)
        coup=saisir_coup(joueur[joueur_courant],situation_courante)
        if coups_possibles!=[] :
            while coup not in coups_possibles:
                coup=saisir_coup(joueur[joueur_courant],situation_courante)
            situation_courante=game.actualiser(coup,situation_courante,joueur_courant)
            game.afficher(situation_courante)
        jeu=game.verification_fin_jeu(situation_courante,joueur_courant)
        joueur_courant=(joueur_courant+1)%2
    phrases=['bravo {} vous avez gagné'.format(joueur[0]),'désolé match nul','bravo {} vous avez gagné'.format(joueur[1])]
    print(phrases[jeu])
                
            
          
#si ce n'est pas un module chargé, importe le module de jeu correspondant à l'argument comme "game" et lance la fonction de jeu 
if __name__ == '__main__':
    if len(sys.argv) < 2:
        module = 'nim'
    else:
        module = sys.argv[1]
    game = __import__(module)
    gestion_jeu()