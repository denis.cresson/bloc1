from random import randint
from copy import deepcopy

def creer_situation_courante():
    """
    retourne une grille vide de jeu de tic tac toe
    :return:(list)
    """
    return [[" " for _ in range(3)]for _ in range(3)]


def afficher(grille):
    """
    affiche la grille de tic tac toe
    :param grille:(list)
    :return:None
    :effet de bord:affichage
    """
    print("   a   b   c\n")
    print("1  {} | {} | {} ".format(*tuple(grille[0])))
    print("  -----------")
    print("2  {} | {} | {} ".format(*tuple(grille[1])))
    print("  -----------")
    print("3  {} | {} | {} ".format(*tuple(grille[2])))
    
def coups_possibles(grille):
    """
    retourne la liste des coup possibles à jouer sous forme "a0"..."c2"
    :param grille:(list)
    :return:(list)
    """
    lettre=["a","b","c"]
    return ["{}{}".format(lettre[j],str(i+1)) for i in range(3) for j in range(3) if grille[i][j]==" "]

def actualiser(coup,grille,joueur_courant):
    """
    retourne la nouvelle grille en complétant avec le coup joué par le joueur courant
    :param coup:(str)
    :param grille:(list)
    :param joueur_courant:(int)
    """
    lettre=["a","b","c"]
    j=lettre.index(coup[0])
    i=int(coup[1])-1
    symboles=["x","o"]
    grille[i][j]=symboles[joueur_courant]
    return grille

def copie_modifiee(coup,grille_de_base,joueur_courant):
    """
    retourne une deepcopie de la nouvelle grille en complétant avec le coup joué par le joueur courant
    :param coup:(str)
    :param grille:(list)
    :param joueur_courant:(int)
    """
    grille=deepcopy(grille_de_base)
    lettre=["a","b","c"]
    j=lettre.index(coup[0])
    i=int(coup[1])-1
    symboles=["x","o"]
    grille[i][j]=symboles[joueur_courant]
    return grille

def ligne(grille,i,lettre):
    """
    teste si la ligne d'indice i de la grille contient 3 fois lettre
    :param grille:(list)
    :param i:(int)
    :param lettre:(str)
    :return:(bool)
    """
    return grille[i].count(lettre)==3

def colonne(grille,i,lettre):
    """
    teste si la colonne d'indice i de la grille contient 3 fois lettre
    :param grille:(list)
    :param i:(int)
    :param lettre:(str)
    :return:(bool)
    """
    return [grille[j][i] for j in range(3)].count(lettre)==3

def diags(grille,lettre):
    """
    teste si l'une des diagonales de la grille contient 3 fois lettre
    :param grille:(list)
    :param lettre:(str)
    :return:(bool)
    """
    return [grille[j][j] for j in range(3)].count(lettre)==3 or [grille[2-j][j] for j in range(3)].count(lettre)==3

def trois(grille):
    """
    teste si l'un des joueurs a gagné
    :param grille:(list)
    :return:(str)"x" ou "o" suivant le joueur gagnant ou "" si personne
    """
    for lettre in ["x","o"]:
        for i in range(3):
            if ligne(grille,i,lettre):
                return lettre
            elif colonne(grille,i,lettre):
                return lettre
        if diags(grille,lettre):
            return lettre
    return ""

def deux(grille,joueur):
    coeff=1
    retour=0
    if joueur==0 or joueur=="ordinateur" or joueur=="ordinateur1" or joueur=="adversaire2":
        coeff=-1
    lettre={-1:"x",1:"o"}
    for coef in [-1,1]:
        for i in range(3):
            if grille[i].count(lettre[coef])==2 and grille[i].count(" ")==1:
                retour+=coeff*coef #on ajoute 1 si meme lettre -1 sinon
            colonne=[grille[j][i] for j in range(3)]
            if colonne.count(lettre[coef])==2 and colonne.count(" ")==1:
                retour+=coeff*coef
        diag1=[grille[j][j] for j in range(3)]
        diag2=[grille[2-j][j] for j in range(3)]
        if diag1.count(lettre[coef])==2 and diag1.count(" ")==1:
            retour+=coeff*coef
        if diag2.count(lettre[coef])==2 and diag2.count(" ")==1:
            retour+=coeff*coef
    return retour
def verification_fin_jeu(grille,joueur):
    """
    teste si le jeu est fini et retourne 1 si match nul 0 si gagnant joueur1 ou 2 si gagnant joueur 2  -1 sinon
    :param grille:(list)
    :param joueur:(int) 0 ou 1
    :return:(int)
    """
    retour=-1
    if joueur==0 or joueur=="ordinateur" or joueur=="ordinateur1" or joueur=="adversaire2":
        indice_joueur=0
    else:
        indice_joueur=2
    if trois(grille)!="":
        retour=indice_joueur
    elif coups_possibles(grille)==[]:
            retour=1
    return retour

def situations_possibles(situation,joueur):
    """
    retourne la liste des situations possibles pour un joueur à partir d'une situation de départ
    :param situation:(list)
    :param joueur:(str)
    :return:(list)
    """
    cp_poss=coups_possibles(situation)
    indice_joueur=1
    if joueur==0 or joueur=="ordinateur" or joueur=="ordinateur1" or joueur=="adversaire2":
        indice_joueur=0
    return [copie_modifiee(coup,situation,indice_joueur) for coup in cp_poss]

def evalu(situation,joueur):
    """ retourne une valeur numérique évaluant la "valeur" d'une situation pour un joueur
    :param situation:(list)
    :param joueur:(str)
    :return:(int)
    """
    lettre="o"
    if joueur==0 or joueur=="ordinateur" or joueur=="ordinateur1" or joueur=="adversaire2":
        lettre="x"
    l=trois(situation)
    if l!="":
        #print(l,lettre)
        if l==lettre:
            retour=1000
        else:
            retour=-1000
    else:
        retour=deux(situation,joueur)
    return retour
    