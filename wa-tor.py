# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 09:46:34 2019

@author: Denis CRESSON
"""
from random import randint
from pylab import *

TEMPS_DE_GESTATION_THON=2
TEMPS_DE_GESTATION_REQUIN=5
ENERGIE_REQUIN=3
POURC_THON=30
POURC_REQUIN=10

th=0
re=0
pasencours=0


def grille_vide(largeur,hauteur):
    """
    construit une grille vide de la bonne taille (la grille contiendra des listes vides)
    :param largeur:(int)
    :param hauteur:(int)
    :CU: None
    
    >>> grille_vide(3,2)
    [[[], [], []], [[], [], []]]
    """
    
    return [[[] for i in range(largeur)] for j in range(hauteur)]

 
def largeur(grille):
    """
    renvoie la largeur d'une grille
    :param grille:(list)
    :return: (int)
    
    >>> largeur(grille_vide(3,2))
    3
    """
    return len(grille[0])

def hauteur(grille):
    """
    renvoie la hauteur d'une grille
    :param grille:(list)
    :return: (int)
    
    >>>hauteur(grille_vide(3,2))
    2
    """
    return len(grille)
    
def nombre_de_cases(grille):
    """
    renvoie le nombre de cases d'une grille
    :param grille:(list)
    :return: (int)
    
    >>> nombre_de_cases(grille_vide(3,2))
    6
    """
    return largeur(grille)*hauteur(grille)

def pos_possibles(grille):
    """
    renvoie une liste des positions (i,j) associés au cases de la grille
    :param grille:(list)
    :return: (int)
    >>> pos_possibles(grille_vide(3, 2))
    [(0,0),(0,1),(1,0),(1,1),(2,0),(2,1)]
    """
    retour=[]
    for i in range(largeur(grille)):
        for j in range(hauteur(grille)):
            retour.append((i,j))
    return retour

def ajoute_un_thon(grille,pos):
    """
    modifie la grille en place en ajoutant un thon à la position donnée
    :param grille:(list)
    :param pos: (tuple) (i,j) avec i et j entiers
    :Effet de bord: modifie la grille
    
    >>> g=grille_vide(4, 3)
    >>> pos=(1,2)
    >>> ajoute_un_thon(g,pos)
    >>> g
    [[[], [], [], []], [[], [], [], []], [[], [2], [], []]]
    """
    grille[pos[1]][pos[0]]=[TEMPS_DE_GESTATION_THON]

def ajoute_un_requin(grille,pos):
    """
    modifie la grille en place en ajoutant un requin à la position donnée
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :Effet de bord: modifie la grille
    
    >>> g=grille_vide(4, 3)
    >>> pos=(2,0)
    >>> ajoute_un_thon(g,pos)
    >>> g
    [[[], [], [5,3], []], [[], [], [], []], [[], [], [], []]]
    """
    grille[pos[1]][pos[0]]=[TEMPS_DE_GESTATION_REQUIN,ENERGIE_REQUIN]

def efface_une_case(grille,pos):
    """
    modifie la grille en place en supprimant la case
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :Effet de bord: modifie la grille
    
    >>> g=grille_vide(4, 3)
    >>> pos=(2,0)
    >>> ajoute_un_thon(g,pos)
    >>> g
    [[[], [], [5,3], []], [[], [], [], []], [[], [], [], []]]
    >>>efface_une_case(g,pos)
    >>>g
    [[[], [], [], []], [[], [], [], []], [[], [], [], []]]
    """
    grille[pos[1]][pos[0]]=[]
        
def remplir_grille(grille,nombre_thon,nombre_requin):
    """
    modifie la grille en place en y ajoutant aléatoirement le nombre de thons et de requins entrés en paramètre puis la retourne
    :param grille:(list)
    :param nombre_thon:(int)
    :param nombre_requin:(int)
    :return: (list) 
    :Effet de bord: modifie la grille
    >>> g=grille_vide(4, 3)
    >>> g
    [[[], [], [], []], [[], [], [], []], [[], [], [], []]]
    >>> remplir_grille(g,2,1)
    [[[2], [], [], []], [[], [], [2], []], [[5, 3], [], [], []]]
    """
    global re,th
    re=nombre_requin
    th=nombre_thon
    positions=pos_possibles(grille)
    for nb_requin in range(nombre_requin):
        indice_case=randint(len(positions))
        coordonnees=positions[indice_case]
        ajoute_un_requin(grille,coordonnees)
        positions.pop(indice_case)
    for nb_thon in range(nombre_thon):
        indice_case=randint(len(positions))
        coordonnees=positions[indice_case]
        ajoute_un_thon(grille,coordonnees)
        positions.pop(indice_case)        
    return grille

def voisins(grille,position):
    """
    renvoie la liste des positions voisines (les quatres)
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (list)
    >>>g=grille_vide(4, 3)
    >>> voisins(g,(2,1))
    [(1, 1), (3, 1), (2, 0), (2, 2)]
    >>> voisins(g,(3,1))
    [(2, 1), (0, 1), (3, 0), (3, 2)]
    >>> voisins(g,(1,0))
    [(0, 0), (2, 0), (1, 2), (1, 1)]
    """
    xpos,ypos=position
    return [((xpos-1)%largeur(grille),ypos),((xpos+1)%largeur(grille),ypos),
            (xpos,(ypos-1)%hauteur(grille)),(xpos,(ypos+1)%hauteur(grille))]

def est_libre(grille,position):
    """
    retourne True si la case est inoccupée et False sinon
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (bool)
    
    >>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>> est_libre(g,(2,1))
    False
    >>> est_libre(g,(1,0))
    True
    """
    return len(grille[position[1]][position[0]])==0

def est_thon(grille,position):
    """
    retourne True si la case est occupée par un thon et False sinon
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (bool)
    
    >>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>> est_thon(g,(2,1))
    True
    >>> est_thon(g,(1,0))
    False
    """
    return len(grille[position[1]][position[0]])==1

def est_requin(grille,position):
    """
    retourne True si la case est occupée par un requin et False sinon
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (bool)
    
    >>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>> est_requin(g,(3,0))
    True
    >>> est_requin(g,(1,0))
    False
    """
    return len(grille[position[1]][position[0]])==2

def voisins_libres(grille,position):
    """
    renvoie la liste des positions voisines inoccupées
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (list)
    >>>g=g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>> voisins_libres(g,(2,1))
    [(1, 1), (2, 0)]
    >>> voisins_libres(g,(3,1))
    []
    >>> voisins_libres(g,(1,0))
    [(2, 0), (1, 2), (1, 1)]
    """
    retour=[]
    xpos,ypos=position
    vois=voisins(grille,position)
    for voi in vois:
        if est_libre(grille,voi):
            retour.append(voi)
    return retour
   
def voisins_thons(grille,position):
    """renvoie la liste des positions voisines occupées par des thons
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (list)
    >>>g=g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>>voisins_thons(g,(2,1))
    [(3, 1)]
    >>>voisins_thons(g,(3,1))
    [(2, 1), (0, 1), (3, 2)]
    >>>voisins_thons(g,(1,0))
    [(0, 0)]
    """
    retour=[]
    vois=voisins(grille,position)
    for voi in vois:
        if est_thon(grille,voi):
            retour.append(voi)
    return retour

def baisse_energie(grille,pos):
    """
    modifie la grille en place en diminuant le taux et retourne le taux
    :param grille:(List)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return:(int) le taux
    >>> g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>> baisse_energie(g,(3,0))
    2
    >>> g
    [[[2], [], [], [5, 2]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    """
    grille[pos[1]][pos[0]][1]-=1
    return grille[pos[1]][pos[0]][1]

def baisse_gestation(grille,pos):
    """
    modifie la grille en place en diminuant le taux et retourne le taux
    :param grille:(List)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return:(int) le taux
    >>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>>baisse_gestation(g,(3,2))
    1
    >>>g
    [[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [1]]]
    """
    grille[pos[1]][pos[0]][0]-=1
    return grille[pos[1]][pos[0]][0]

def deplacer(grille,posd,posf):
    """
    modifie la grille en place en déplaçant une case de la grille de la première position à la seconde et efface la première case
    :param grille:(list)
    :param posd:(tuple) (i,j) avec i et j entiers position de départ
    :param posf:(tuple) (i,j) avec i et j entiers position de fin
    :Effet de bord: modifie la liste
    >>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>>deplacer(g,(3,0),(3,1))
    >>>g
    >>>[[[2], [], [], []], [[2], [], [2], [5,3]], [[5, 3], [], [5, 3], [2]]]
    >>> deplacer(g,(3,2),(3,0))
    >>> g
    [[[2], [], [], [2]], [[2], [], [2], [5, 3]], [[5, 3], [], [5, 3], []]]
    """
    adeplacer=grille[posd[1]][posd[0]]
    grille[posf[1]][posf[0]]=adeplacer
    efface_une_case(grille,posd)
    
def position_aleatoire(liste):
    """
    retourne une position aléatoire parmis une liste de positions fournie en paramètre
    :param liste:(List) liste de tuples
    :return:(tuple) une position
    >>> liste=[(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2), (3, 0), (3, 1), (3, 2)]
    >>> position_aleatoire(liste)
    (1, 2)
    """
    indice=randint(len(liste))
    p=liste[indice]
    return p

def comportement_thon(grille,position):
    """
    modifie la grille en place pour prendre en compte le comportement du thon
    :param grille:(List)
    :param position:(tuple)
    :Effet de bord: modifie la grille
    """
    global th
    deplace=False
    nouvelle_position=position
    positions_possibles=voisins_libres(grille,position)
    if len(positions_possibles)!=0:
        nouvelle_position=position_aleatoire(positions_possibles)       
        deplacer(grille,position,nouvelle_position)       
        deplace=True
    gestation=baisse_gestation(grille,nouvelle_position)
    if gestation==0:
        if deplace:
            th+=1
            ajoute_un_thon(grille,position)
        ajoute_un_thon(grille,nouvelle_position)
        
def comportement_requin(grille,position):
    """
    modifie la grille en place pour prendre en compte le comportement du requin
    :param grille:(List)
    :param position:(tuple)
    :Effet de bord: modifie la grille
    """
    global re,th
    deplace=False
    nouvelle_position=position
    energie=baisse_energie(grille,position)
    positions_thons=voisins_thons(grille,position)
    positions_possibles=voisins_libres(grille,position)
    if len(positions_thons)!=0:
        th-=1
        nouvelle_position=position_aleatoire(positions_thons)
        deplacer(grille,position,nouvelle_position)
        grille[nouvelle_position[1]][nouvelle_position[0]][1]=ENERGIE_REQUIN
        deplace=True
    elif len(positions_possibles)!=0:
        nouvelle_position=position_aleatoire(positions_possibles)
        deplacer(grille,position,nouvelle_position)
        deplace=True
    if energie==0:
        efface_une_case(grille,nouvelle_position)
        re-=1
    else:
        gestation=baisse_gestation(grille,nouvelle_position)
        if gestation==0:  
            if deplace:
                re+=1
                ajoute_un_requin(grille,position)
                grille[nouvelle_position[1]][nouvelle_position[0]][0]=TEMPS_DE_GESTATION_REQUIN

def pas_suivant(grille):
    """
    modifie la grille en place en choisissant une case puis en lui appliquant le comportement adapté
    :param grille:(List)
    :Effet de bord: modifie la grille
    """
    global pasencours
    pasencours+=1
    position=position_aleatoire((pos_possibles(grille)))
    if est_thon(grille,position):
        comportement_thon(grille,position)
            
    if est_requin(grille,position):
        comportement_requin(grille,position)
                    

def previsualisation(grille,i,l,h,p):
    """
    preaffiche la grille en utilisant des pylab(sans show)
    :param grille:(list)
    :param i:(int) le pas
    :param l:(int) la largeur
    :param h:(int) la hauteur
    :param p:(int) le nombre de pas
    :CU:None
    """
    abst=[]
    ordt=[]
    absr=[]
    ordr=[]
    title('simulation pour largeur:'+str(l)+' hauteur:'+str(h)+' pas:'+str(p) +'\n'+'iteration:'+str(i))
    axis([-1,len(grille[0]),-1,len(grille)])
    for h in range(len(grille)):
        for l in range(len(grille[0])):
            if len(grille[h][l])==1:
                abst.append(l)
                ordt.append(len(grille)-h-1)
    plot(abst, ordt,"bo");
    for h in range(len(grille)):
        for l in range(len(grille[0])):
            if len(grille[h][l])==2:
                absr.append(l)
                ordr.append(len(grille)-h-1)
    plot(absr, ordr,"ro");
    draw()

def visualisation_mer(largeur,hauteur,pas):
    """
    crée une grille de la bonne taille avec le bon pourcentage de poissons
    puis la fait évoluer le bon nombre de fois en appellant la prévisualisation  tout les 100 pas
    et pour finir affiche la visualisation
    :param largeur:(int)
    :param hauteur:(int)
    :param pas:(int)
    """
    taille=largeur*hauteur
    grille=remplir_grille(grille_vide(largeur,hauteur),(taille*POURC_THON)//100,(taille*POURC_REQUIN)//100)
    previsualisation(grille,0,largeur,hauteur,pas)
    clf()
    for i in range(pas*nombre_de_cases(grille)):
        pas_suivant(grille)
        if i%100==0:
            previsualisation(grille,i,largeur,hauteur,pas)
            pause(0.1)
            clf()
    show();
    
def simulation(largeur,hauteur,pas):
    """
    crée une grille de la bonne taille avec le bon pourcentage de poissons puis affiche le graphique des effectifs de thon et de requins au cours de la simulation
    :param largeur:(int)
    :param hauteur:(int)
    :param pas:(int)
    """
    taille=largeur*hauteur
    grille=remplir_grille(grille_vide(largeur,hauteur),(taille*POURC_THON)//100,(taille*POURC_REQUIN)//100)
#    axis([-1,len(grille[0]),-1,len(grille)])
    ths=[th]
    res=[re]
    lesx=[pasencours]
    for i in range(pas*nombre_de_cases(grille)):
        pas_suivant(grille)
        ths.append(th)
        res.append(re)
        lesx.append(pasencours)
    plot(lesx,ths,"b-");
    plot(lesx,res,"r-");
    axis([-1,len(lesx),-1,max(max(ths),max(res))])
    title('simulation pour largeur:'+str(largeur)+' hauteur:'+str(hauteur)+' pas:'+str(pas))
    show();
