#!/usr/bin/python3
# -*- coding: utf-8 -*-

#importation des modules Time et Competitor

import Competitor as Comp
import Time
from tris import tri_rapide

def read_competitors(nom_fichier):
    """
    retourne le dictionaire des compétiteurs inscrits dans le fichier dont le nom est en paramètre
    :param nom_fichier:(str)
    :return:(dict) dictionaire de Competitor
    """
    
    resultats={}
    try:
        with open(nom_fichier,"r",encoding="utf8") as fich_in:
            fich_in.readline()
            bib_num=0
            for ligne in fich_in:
                liste=ligne.split(";")
                liste[3]=liste[3].rstrip("\n")
                bib_num+=1
                resultats[bib_num]=Comp.create(*liste,bib_num)
    except FileNotFoundError:
        print("le fichier n'existe pas")
    return resultats

def affichage_liste_competiteurs(liste):
    """
     affiche sur la sortie standard chacune de ces données à raison d'une par ligne
    :param liste:(list)
    :return: None
    :effet de bord: affiche le contenu de la liste
    """
    
    for comp in liste:
        print(Comp.to_string(comp))

class DossardAbsent(Exception):
    pass

def select_competitor_by_bib(dictio,bib):
    """
    retourne le compétiteur du dictionaire dont le numéro de dossard est passé en paramètre
    :param dictio:(dict) de Competitors
    :param bib:(int)
    :return: (Competitor)
    
    """
    for key,comp in dictio.items():
        if Comp.get_bib_num(comp)==bib:
            return comp
    raise DossardAbsent

class AnnéeAbsente(Exception):
    pass

def select_competitor_by_birth_year (dictio,birth_year):
    """
    retourne la liste des compétiteurs du dictionaire dont l'année de naissance est passée en paramètre
    :param dictio:(dict) de Competitors
    :param birth_date:(str)
    :return: (list)
    
    """
    resultats=[]
    for _,comp in dictio.items():
        if Comp.get_birthdate(comp).endswith(birth_year):
            resultats.append(comp)
    if resultats==[]:
        raise AnneeAbsente
    return resultats

class ChaineAbsente(Exception):
    pass

def select_competitor_by_name (dictio,chaine):
    """
    retourne la liste des compétiteurs du dictionaire dont le nom contient la chaine passée en paramètre
    :param dictio:(dict) de Competitors
    :param chaine:(str)
    :return: (list)
    
    """
    resultats=[]
    for _,comp in dictio.items():
        if chaine in Comp.get_lastname(comp):
            resultats.append(comp)
    if resultats==[]:
        raise ChaineAbsente
    return resultats

def read_performances(nom_fichier):
    """
    retourne un dictionaires des performances associées aux dossards du fichier dont le nom est en paramètre
    :param nom_fichier:(str)
    :return:(dict) valeur Time
    """
    
    resultats={}
    try:
        with open(nom_fichier,"r",encoding="utf8") as fich_in:
            fich_in.readline()
            for ligne in fich_in:
                liste=ligne.split(";")
                bib,heure,minute,seconde=tuple(liste)
                resultats[int(bib)]=Time.create(int(heure),int(minute),int(seconde))
    except FileNotFoundError:
        print("le fichier n'existe pas")
    return resultats

def set_performances(dict_competitors,dict_perfs):
    """
    ajoute les performances lues dans dict_perfs aux compétiteurs du dictionaire de compétiteurs
    :param dict_competitors:(dict)
    :param dict_perfs:(dict)
    :effet de cord: dict_competitors modifié en place
    """
    for bib in dict_perfs:
        Comp.set_performance(dict_competitors[bib],Time.to_string(dict_perfs[bib]))
        
def print_result(list_competitors):
    """
    affiche les compétiteurs avec leurs performances
    :param list_competitors:(list)
    :effet de bord: affichage sur la sortie standant
    """
    for comp in list_competitors:
        if comp['performance']!=None:
            print(f"{Comp.to_string(comp):<50} => {comp['performance']}")
        else:
            print(f"{Comp.to_string(comp):<50} => ")

def save_result(dict_competitors,nom_fichier):
    """
    enregistre le dictionaire de compétiteurs dans un fichier dont le nom est en parametre au format csv
    :param dict_competitors:(dict)
    :param nom_fichier:(str)
    :effet de bord: creation du fichier ou modification
    """
    with open(nom_fichier,"w",encoding="utf8") as fich_out:
        fich_out.write("Num_dossard;Prénom;Nom;Performance\n")
        for _,comp in dict_competitors.items():
            ligne=str(Comp.get_bib_num(comp))+";"+";".join(list(comp.values())[1:-3])+";"
            if comp['performance']!=None:
                ligne+=comp['performance']
            fich_out.write(ligne+"\n")
def test():
    """
    crée les dictionaires de performances et competiteurs ajoute les performances et les affiche (pour la version small)
    """
    p=read_performances("../data/small_performances.csv")
    c=read_competitors("../data/small_inscrits.csv")
    set_performances(c,p)
    print_result(c)

def compare_lastname(competitors1, competitors2):
    """
    :param x, y: (Competitors) 
    :return: (int) 
      - -1 si lastname1 avant lastname2
      - 0 si même lastname
      - 1 si lastname1 après lastname2
    """
    
    lastname1=Comp.get_lastname(competitors1)
    lastname2=Comp.get_lastname(competitors2)

    if  lastname1< lastname2:
        return -1
    elif lastname1 > lastname2:
        return 1
    else:
        return 0

def compare_perf(competitors1, competitors2):
    """
    :param x, y: (Competitors) 
    :return: (int) 
      - -1 si perf1 avant pef2
      - 0 si même perf
      - 1 si pef1 après perf2
    """
    if Comp.get_performance(competitors1)!=None:
        h1,m1,s1=int(Comp.get_performance(competitors1)[0:-6]),int(Comp.get_performance(competitors1)[-5:-3]),int(Comp.get_performance(competitors1)[-2:])
    else :
        return 1
    if Comp.get_performance(competitors2)!=None:
        h2,m2,s2=int(Comp.get_performance(competitors2)[0:-6]),int(Comp.get_performance(competitors2)[-5:-3]),int(Comp.get_performance(competitors2)[-2:])
    else:
        return -1
    perf1=Time.create(h1,m1,s1)
    perf2=Time.create(h2,m2,s2)

    return Time.compare(perf1,perf2)
    
    
def sort_competitors_by_lastname(dict_competitors):
    """
    retourne une liste triée des compétiteurs du dictionaire par nom
    :param dict_competitors:(dict)
    :return:(list)
    """
    liste=[]
    for key in dict_competitors:
        liste.append(dict_competitors[key])
    tri_rapide(liste,comp=compare_lastname)
    return liste

def sort_competitors_by_perf(dict_competitors):
    """
    retourne une liste triée des compétiteurs du dictionaire par performance
    :param dict_competitors:(dict)
    :return:(list)
    
    """
    liste=list(dict_competitors.values())
    tri_rapide(liste,comp=compare_perf)
    return liste

def test_trie_nom():
    """
    crée les dictionaires de performances et competiteurs ajoute les performances les trie par nom et les affiche (pour la version small)
    """
    p=read_performances("../data/small_performances.csv")
    c=read_competitors("../data/small_inscrits.csv")
    set_performances(c,p)
    print_result(sort_competitors_by_lastname(c))

def test_trie_perf():
    """
    crée les dictionaires de performances et competiteurs ajoute les performances les trie par nom et les affiche (pour la version small)
    """
    p=read_performances("../data/performances.csv")
    c=read_competitors("../data/inscrits.csv")
    set_performances(c,p)
    print_result(sort_competitors_by_perf(c))

def select_competitor(dictio,predicat):
    """
    retourne la liste des compétiteurs du dictionaire verifiant le prédicat passé en paramètre
    :param dictio:(dict) de Competitors
    :param predicat:(function)
    :return: (list)
    >>> c=read_competitors("../data/inscrits.csv")
    >>> print_result(select_competitor(c,name('Beausoleil')))
    [485]: Mandel Beausoleil (M - 20/5/1998)           => 
    [582]: Lucas Beausoleil (M - 3/2/1970)             =>
    >>> c=read_competitors("../data/inscrits.csv")
    >>> print_result(select_competitor(c,birth_year('1970')))
    [61]: Vignette Mathieu (F - 13/4/1970)             => 
    [197]: Élise Faure (F - 23/10/1970)                => 
    [262]: Cammile Turgeon (F - 17/12/1970)            => 
    [285]: David Arcouet (M - 10/9/1970)               => 
    [292]: Millard Grivois (M - 17/7/1970)             => 
    [306]: Éléonore Durand (F - 1/2/1970)              => 
    [329]: Sidney Robert (M - 21/7/1970)               => 
    [360]: Mignonette Pouliotte (F - 15/2/1970)        => 
    [461]: Campbell Boucher (M - 31/8/1970)            => 
    [492]: Berangaria Lanteigne (F - 5/12/1970)        => 
    [540]: Simone Mathieu (F - 29/10/1970)             => 
    [582]: Lucas Beausoleil (M - 3/2/1970)             => 
    [969]: Christian Chastain (M - 19/3/1970)          => 
    [993]: Bellamy Bordeaux (M - 2/8/1970)             =>
    >>> c=read_competitors("../data/small_inscrits.csv")
    >>> print_result(select_competitor(c,femme))
    [6]: Romaine Hughes (F - 17/10/1943)               => 
    [9]: Avelaine CinqMars (F - 14/2/1983)             => 
    """
    resultats=[]
    for _,comp in dictio.items():
        if predicat(comp):
            resultats.append(comp)
    return resultats

def birth_year(year):
    """
    retourne une fonction de predicat verifiant la date passée en paramètre
    :param year:(str)
    :return:(function) retourne une fonction de predicat verifiant la date de naissance du competiteur
                        :param comp:(Comp)
                        :return:(bool)
    """
    return lambda comp :Comp.get_birthdate(comp).endswith(year)

def name(nom):
    """
    retourne une fonction de predicat verifiant le nom passée en paramètre
    :param nom:(str)
    :return:(function) retourne une fonction de predicat verifiant le nom du competiteur
                        :param comp:(Comp)
                        :return:(bool)
    """
    return lambda comp :Comp.get_lastname(comp)==nom

def femme(comp):
    """
    retourne une fonction de predicat verifiant le nom du compétiteur
    :param comp:(Comp)
    :return:(bool)
    """
    return Comp.get_sex(comp)=="F"
