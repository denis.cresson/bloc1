#!/usr/bin/python3
# -*- coding: utf-8 -*-

#importation de la class namedtuple du module   collection

from collections import namedtuple

#création du type Time à partir de named tuple
Time = namedtuple("Time"," hours,minutes,seconds")

def create(h,m,s):
    """
    construit une donnée de type Time à partir de trois arguments
    :param h:(int)
    :param m:(int)
    :param s:(int)
    :return: (Time)
    """
    try:
        assert type(s)==int and  type(m)==int and type(h)==int, 'il faut des entiers'
        assert s<60, 'il faut un nombre de secondes inférieur à 60'
        assert m<60, 'il faut un nombre de minutes inférieur à 60'
        return Time(h,m,s)
    except AssertionError as err:
        print(err)

def compare(t1,t2):
    """
    définit une relation d'ordre sur les données de type Time.
    le résultat de cette fonction, à deux paramètres de type Time, est :
        négatif si son premier paramètre est inférieur au second,
        positif s'il lui est plus grand
        nul quand ils sont égaux.
    :param t1:(Time)
    :param t2:(Time)
    :return:(int)
    """
    return (t1.hours-t2.hours)*3600+(t1.minutes-t2.minutes)*60+t1.seconds-t2.seconds

def to_string(t):
    """
     definit une représentation sous la forme d'une chaîne de caractères de
     son paramètre de type Time
     :param t:(Time)
     :return:(str)
     """
    return f"{t.hours:>2}h{t.minutes:02d}m{t.seconds:02d}"