def somme(a,b):
    if a==0:
        return b
    else:
        return somme(a-1,b+1)
    
def produit(a,b):
    if a==0:
        return 0
    else:
        return b+ produit(a-1,b)
    
def puissance(a,n):
    if n==0:
        return 1
    else :
        return a*puissance(a,n-1)
    
def pgcd(a,b):
    
    if a%b==0:
            return b
    else :
        return pgcd(b,a%b)
def palind(chaine):
    if len(chaine)<2:
        return True
    else :
        if chaine[0]==chaine[-1]:
            return palind(chaine[1:-1])
        else :
            return False

def romain_to_arabe(chaine):
    VALEUR_ROMAIN = { 'M' : 1000, 'D' : 500, 'C' : 100, 'L' : 50, 'X' : 10, 'V' : 5, 'I' : 1}
    if len(chaine)==1:
        return VALEUR_ROMAIN[chaine[0]]
    else :
        if VALEUR_ROMAIN[chaine[1]]<=VALEUR_ROMAIN[chaine[0]]:
            return VALEUR_ROMAIN[chaine[0]]+romain_to_arabe(chaine[1:])
        else :
            return -VALEUR_ROMAIN[chaine[0]]+romain_to_arabe(chaine[1:])
        
def permute (chaine):
    if len(chaine)==1:
        return [chaine]
    else :
        result=[]
        for i in range(len(chaine)):
            #un charactère est choisit
            for p in permute(chaine[:i]+chaine[i+1:]):
                #une permutation de la chaine des caractères restant est choisit
                result.append(chaine[i]+p)
    return result

def somme_liste(liste):
    if liste==[]:
        return 0
    else:
        return liste[0]+somme_liste(liste[1:])
class listeVide(Exception):
    pass

def last(liste):
    if liste==[]:
        raise listeVide
    if liste[1:]==[]:
        return liste[0]
    else:
        return last(liste[1:])
    
def concat(liste1,liste2):
    if liste1==[]:
        return liste2
    else:
        return [liste1[0]]+concat(liste1[1:],liste2)

def carre(x):
    return x**2

def map(f,liste):
    if liste==[]:
        return []
    else:
        return [f(liste[0])]+map(f,liste[1:])

def shuffle(liste1,liste2):
    if liste1==[]:
        return liste2
    else:
        return [liste1[0]]+shuffle(liste2,liste1[1:])

def deplace(L=[5,4,3,2,1],deb=1,fin=3):
    if len(L)==1:
        bouge(L[0],deb,fin)
    else:
        positions=[1,2,3]
        positions.remove(deb)
        positions.remove(fin)
        autre=positions[0]
        deplace(L[1:],deb,autre)
        bouge(L[0],deb,fin)
        deplace(L[1:],autre,fin)

def bouge(pion,deb,fin):
    print(f"bouger le pion de taille {pion} de la colonne {deb} à la colonne {fin}")
    

    
