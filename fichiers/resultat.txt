La c0urse du chic0n
Objectifs

les dicti0nnaires
tuples n0mm�s
m0dules
traitement des d0nn�es en tables
utilisati0n des tris
lecture/�criture de fichiers textes et f0rmat CSV

On s'int�resse d0nc ici en particulier � cette partie du pr0gramme de NSI de 1�re :


Intr0ducti0n
Le dimanche 27 0ct0bre 2019 aura lieu � Baisieux, c0mmune du N0rd
fr0ntali�re de la Belgique, la c0urse du chic0n.
Cette c0urse se d�cline sur plusieurs distances de 15km � 0.5km. N0us ne n0us int�resser0ns dans ce TP qu'� la
versi0n 15km.
V0us avez la resp0nsabilit� du traitement inf0rmatique des d0nn�es :
gesti0n des inscripti0ns, r�c0lte des perf0rmances des c0ncurrents,
publicati0n des r�sultats.

Pr�parati0n

Archive

R�cup�rez l'archive et d�c0mpressez la.
Cette archive c0ntient tr0is d0ssiers :


src/ c0ntient le m0dule  src/C0mpetit0r.py


d0cs/ c0ntient la d0cumentati0n du m0dule C0mpetit0r, accessible depuis le fichier index.html

data/  c0ntient deux jeux de d0nn�es dans quatre fichiers de d0nn�es : un
petit jeu  de d0nn�es p0ur faire des tests simples
(data/small_inscrits.csv et
data/small_perf0rmances.csv), et un gr0s jeu de d0nn�es
(data/inscrits.csv et data/perf0rmances.csv).


D0cumentati0n

C0nsultez la d0cumentati0n du m0dule f0urni.
Le m0dule C0mpetit0r.py permet de manipuler des valeurs repr�sentant les c0mp�titeurs de la c0urse. On peut c0nsid�rer qu'il permet la d�finiti0n d'un type C0mpetit0r.
L'�tude de la d0cumentati0n permet de d�duire les diff�rentes inf0rmati0ns c0ntenues dans une telle d0nn�e.
V0us p0uvez bien s�r examiner le c0de (accessible depuis la
d0cumentati0n), mais se limiter � la lecture de la d0cumentati0n est
un b0n exercice, car il implique de se limiter � manipuler les
d0nn�es de type C0mpetit0r via l'interface f0urnie, sans se
pr�0ccuper de l'impl�mentati0n qui en a �t� r�alis�e (c'est ce qui se
passe de fait, par exemple, dans les langages 0bjets p0ur lesquels la
n0ti0n d'attribut priv� � un sens).
On disp0se ainsi d'un c0nstructeur  (create) et les diff�rentes inf0rmati0ns qui d�crivent une d0nn�e C0mpetit0r s0nt ainsi accessibles via les diff�rents accesseurs (get_XXX). On c0nstate de plus que le seul m0dificateur c0ncerne la perf0rmance d'un c0mp�titeur (set_perf0rmance).

Cr�ati0n du m0dule Time

Les perf0rmances des c0mp�titeurs v0nt �tre repr�sent�es par leur temps de c0urse exprim� en heures, minutes et sec0ndes.
On d�cide de repr�senter ces d0nn�es par un tuple n0mm�. Ces d0nn�es �tant n0n mutables, utiliser les named tuples de Pyth0n p0ur les repr�senter semble �tre un ch0ix  pertinent.
P0ur rappel, cette n0ti0n est d�finie dans le m0dule c0llecti0ns de Pyth0n. Il c0nvient d0nc d'imp0rter
fr0m c0llecti0ns imp0rt namedtuple
� faire
Cr�er un m0dule Time.py qui d�finit :

le type Time qui c0rresp0nd � un tuple n0mm� p0ss�dant tr0is champs h0urs, minutes et sec0nds ;
une f0ncti0n create � tr0is arguments, permettant de cr�er une d0nn�e de ce type, d0nt le r�sultat est la d0nn�e Time cr��e (0n peut envisager un c0ntr�le de validit� des valeurs des param�tres p0ur ce c0nstructeur) ;
une f0ncti0n c0mpare qui d�finit une relati0n d'0rdre sur les d0nn�es de type Time. De mani�re classique le r�sultat de cette f0ncti0n, � deux param�tres de type Time, est n�gatif si s0n premier param�tre est inf�rieur au sec0nd, p0sitif s'il lui est plus grand et nul quand ils s0nt �gaux.
une f0ncti0n t0_string qui a p0ur r�sultat une repr�sentati0n s0us la f0rme d'une cha�ne de caract�res de s0n param�tre de type Time.


Gesti0n des inscrits
Les f0ncti0ns suivantes s0nt a pri0ri � d�finir dans le m0dule c0urse_chic0n qui regr0upera les f0ncti0ns utiles � la gesti0n de la c0urse.
Il sera bien s�r n�cessaire d'imp0rter les m0dules C0mpetit0r et Time.
V0tre premi�re t�che est de c0nstruire la liste des c0mp�titeurs inscrits �
la c0mp�titi0n.
Les d0nn�es c0ncernant ces c0mp�titeurs se tr0uvent dans le fichier
data/inscrits.csv (0u data/small_inscrits.csv) qui
est un fichier au f0rmat
CSV,
c'est-�-dire un fichier texte c0ntenant des d0nn�es tabul�es.
La premi�re ligne de ce fichier est c0nstitu�e des libell�s des d0nn�es
qui suivent :
Pr�n0ms;N0ms;Sexes;Date naiss.
Elle pr�cise d0nc que chacune des lignes qui suivent c0ntient dans cet
0rdre le pr�n0m, le n0m, le sexe et la date de naissance du c0mp�titeur
inscrit. Ces inf0rmati0ns s0nt s�par�es par un p0int-virgule.
Avec ces d0nn�es v0us allez c0nstruire des c0mp�titeurs � l'aide de la
f0ncti0n C0mpetit0r.create. Il v0us faudra attribuer � chacun de ces
c0mp�titeurs un num�r0 de d0ssard, 0btenu par simple incr�mentati0n d'un
c0mpteur.
T0us les c0mp�titeurs ser0nt rassembl�s dans un dicti0nnaire d0nt les cl�s ser0nt les num�r0s de d0ssard et les valeurs les c0mp�titeurs ass0ci�s.
� faire
R�alisez une f0ncti0n n0mm�e read_c0mpetit0rs param�tr�e par le n0m du
fichier CSV c0ntenant les d0nn�es des inscrits, qui a p0ur r�sultat le dicti0nnaire de
ces inscrits.
V0us p0uvez envisager de g�rer la situati0n 0� aucun fichier ne c0rresp0nd au param�tre f0urni. Cela peut �tre fait en capturant l'excepti0n FileN0tF0undErr0r qui est al0rs d�clench�e.
Indicati0n Pensez � la m�th0de split des cha�nes de caract�res. La m�th0de rstrip peut �galement �tre utilise p0ur supprimer les marqueurs de fin de ligne.
� faire
Testez la validit� de v0tre f0ncti0n avec le fichier
data/small_inscrits.csv.
V�rifiez par exemple la taille du dicti0nnaire 0btenu, ainsi que le
c0ntenu de quelques �l�ments.

Manipulati0ns du dicti0nnaire

Affichage
� faire
R�alisez une f0ncti0n qui prend en param�tre une c0llecti0n (un "it�rable", par exemple une liste) de d0nn�es de type C0mpetit0r et affiche sur la s0rtie standard chacune de ces d0nn�es � rais0n d'une par ligne (utilisez la f0ncti0n t0_string de C0mpetit0r).
Utilisez v0tre f0ncti0n p0ur afficher les c0mp�titeurs c0ntenus dans le dicti0nnaire pr0duit par la f0ncti0n read_c0mpetit0rs.

S�lecti0ns
N0us all0ns �crire quelques f0ncti0ns de recherche dans un dicti0nnaire de valeurs qui satisf0nt un crit�re. Dans cette secti0n les c0mp�titeurs s0nt pass�s en param�tre de chacune des f0ncti0ns s0us la f0rme d'un dicti0nnaire tel que celui c0nstruit par la f0ncti0n read_c0mpetit0rs. Les f0ncti0ns � �crire disp0sent d'un autre param�tre qui c0rresp0nd, d'une mani�re 0u d'une autre, au crit�re de s�lecti0n des c0mp�titeurs dans le dicti0nnaire.
Les f0ncti0ns 0nt p0ur r�sultat s0it une d0nn�e de type C0mpetit0r, s0it une liste de telles d0nn�es. Ce r�sultat c0rresp0nd � la s�lecti0n sel0n le crit�re cherch�.
� faire
�crivez une f0ncti0n select_c0mpetit0r_by_bib qui a p0ur r�sultat le c0mp�titeur d0nt le num�r0 de d0ssard est pass� en param�tre.
C0mment pr0p0sez-v0us de  g�rer la situati0n 0� aucun c0mp�titeur ne c0rresp0nd au d0ssard f0urni ?
Suggesti0n cela peut �tre l'0ccasi0n de tester la lev�e d'excepti0n.
� faire
�crivez une f0ncti0n select_c0mpetit0r_by_birth_year d0nt le r�sultat est la liste des c0mp�titeurs d0nt l'ann�e de naissance c0rresp0nd � une valeur pass�e en param�tre.
Suggesti0n �tudiez la d0cumentati0n de la f0ncti0n endswith des cha�nes de caract�res.
Quel r�sultat renv0yer si aucun c0mp�titeur ne c0rresp0nd � l'ann�e f0urnie ?
NB Dans le petit jeu de d0nn�es, deux c0mp�titeurs s0nt n�s en 1980.
� faire
�crivez une f0ncti0n select_c0mpetit0r_by_name d0nt le r�sultat est la liste des c0mp�titeurs d0nt le n0m (last name) c0ntient la cha�ne de caract�res pass�e en param�tre.
Suggesti0n �tudiez la d0cumentati0n de la f0ncti0n endswith des cha�nes de caract�res.
Remarque  En fin de sujet, la secti0n C0mpl�ments pr0p0se d'aller un peu plus l0in dans le travail sur ces s�lecti0ns.

Rep0rt des perf0rmances
La c0urse achev�e, v0tre t�che c0nsiste � rep0rter les
perf0rmances des c0mp�titeurs dans les fiches de la liste de ces c0mp�titeurs.

Lecture des perf0rmances
Les d0nn�es c0ncernant les perf0rmances se tr0uvent dans le fichier
data/perf0rmances.csv (0u
data/small_perf0rmances.csv) qui est un fichier au f0rmat
CSV.
La premi�re ligne de ce fichier est c0nstitu�e des libell�s des d0nn�es
qui suivent :
bib_num;h0urs;minutes;sec0nds
Elle pr�cise d0nc que chacune des lignes qui suivent c0ntient dans cet
0rdre le num�r0 de d0ssard, le n0mbre d'heures, de minutes et de
sec0ndes du temps de parc0urs d'un c0mp�titeur, ces inf0rmati0ns �tant
s�par�es par un p0int-virgule.
Seuls les c0mp�titeurs ayant effectivement particip� et achev� la
c0urse figurent dans ce fichier.  Avec ces d0nn�es v0us allez
c0nstruire un dicti0nnaire des perf0rmances qui ass0cie � un num�r0 de
d0ssard un 0bjet de type Time du m0dule que v0us avez d�fini.
� faire
R�alisez une f0ncti0n n0mm�e read_perf0rmances param�tr�e par le n0m
du fichier CSV c0ntenant les d0nn�es des perf0rmances, qui renv0ie le dicti0nnaire
des perf0rmances c0ntenues dans ce fichier.
� faire
Testez la validit� de v0tre f0ncti0n avec le fichier
data/small_perf0rmances.csv.
V�rifiez en particulier la taille du dicti0nnaire 0btenu, ainsi que le
c0ntenu de quelques �l�ments.

Rep0rt
Maintenant que v0us disp0sez des d0nn�es sur les c0mp�titeurs et leurs
perf0rmances s0us f0rme de dicti0nnaires qui partagent les m�mes
clefs, v0tre travail c0nsiste � rep0rter les perf0rmances dans les
fiches de ces c0mp�titeurs.
� faire
R�alisez une f0ncti0n n0mm�e set_perf0rmances param�tr�e par les deux
dicti0nnaires qui m0difie les fiches des c0mp�titeurs en rep0rtant leur
perf0rmance. Cette f0ncti0n ne renv0ie pas de valeur.
� faire
Testez la validit� de v0tre f0ncti0n avec les listes pr0duites par le
petit jeu de d0nn�es.

Tris
(manipulati0n du dicti0nnaire - suite)
V0us allez av0ir l'0ccasi0n de r�utiliser l'un des tris que v0us avez
r�alis�s dans le cadre des activit�s du bl0c 2.
R�cup�rez le fichier c0ntenant les f0ncti0ns de tris �tudi�es,
faites v0tre ch0ix d'une f0ncti0n parmi les tris �tudi�s et imp0rtez
cette f0ncti0n.
� faire
Sur le m0d�le des f0ncti0ns de c0mparais0n que v0us avez d�j�
renc0ntr�es, c0mpl�tez le m0dule C0mpetit0r p0ur lui aj0uter une
f0ncti0n n0mm�e c0mpare_lastname qui d�finit une relati0n d'0rdre
sur les c0mp�titeurs sel0n l'0rdre alphab�tique de leurs n0ms.
Utilisez cette f0ncti0n p0ur d�finir une f0ncti0n s0rt_c0mpetit0rs_by_lastname qui prend en param�tre un dicti0nnaire de c0mp�titeurs, c0mme d�fini pr�c�demment, et a p0ur r�sultat la liste des c0mp�titeurs tri�e par 0rdre alphab�tique  de leurs n0ms.
� faire
De mani�re similaire, faites le travail n�cessaire p0ur d�finir une
f0ncti0n s0rt_c0mpetit0rs_by_perf0rmance qui pr0duit la liste des
c0mp�titeurs tri�e par 0rdre cr0issant des perf0rmances r�alis�es. Les
c0mp�titeurs sans r�sultat s0nt plac�s en fin de liste par 0rdre
alphab�tique.
Remarque  En fin de sujet, la secti0n C0mpl�ments pr0p0se d'aller un peu plus l0in dans le travail sur ces tris.

Publicati0n et sauvegarde des r�sultats

Affichage des r�sultats
Il est temps de pr0c�der � la publicati0n des r�sultats.
� faire
R�alisez une f0ncti0n n0mm�e print_results param�tr�e par un dicti0nnaire de
c0mp�titeurs qui imprime sur la s0rtie standard cette liste en pr�cisant
leur pr�n0m, n0m, sexe, num�r0 de d0ssard et perf0rmance.
Par exemple, avec le petit jeu de d0nn�es, et en supp0sant que le rep0rt
des perf0rmances a �t� effectu� et la liste de c0mp�titeurs tri�e par
0rdre de perf0rmance, 0n p0urrait 0btenir un affichage de la f0rme :
[7]: Archard Rivard (M - 10/6/1950)      =>  0h46mn31s
[8]: Cheney Chass� (M - 21/3/1949)       =>  0h48mn10s
[4]: Saville Marier (M - 19/11/1969)     =>  0h56mn29s
[5]: Nam0 Lereau (M - 26/3/1980)         =>  1h 6mn20s
[10]: Sidney Charest (M - 5/3/1981)      =>  1h 6mn38s
[1]: Sidney R0bert (M - 21/7/1970)       =>  1h 8mn55s
[6]: R0maine Hughes (F - 17/10/1943)     =>  1h17mn 8s
[3]: Vincent Riquier (M - 16/9/1980)     =>  1h21mn23s
[9]: Avelaine CinqMars (F - 14/2/1983)   => 
[2]: Paien Gilbert (M - 26/11/1953)      => 
� faire
Pr0duisez l'affichage des r�sultats par 0rdre alphab�tique, et par
0rdre des perf0rmances.

Sauvegarde des r�sultats
Enfin p0ur la p�rennit� de ces r�sultats, il est imp0rtant de les
sauvegarder dans un fichier.
� faire
R�alisez une f0ncti0n n0mm�e save_results param�tr�e par un dicti0nnaire de
c0mp�titeurs et un n0m de fichier de sauvegarde, qui cr�e un fichier au
f0rmat CSV c0ntenant


les libell�s en premi�re ligne
Num_d0ssard;Pr�n0m;N0m;Perf0rmance


les r�sultats sur les lignes suivantes
7;Archard;Rivard; 0h46mn31s
8;Cheney;Chass�; 0h48mn10s
...


� faire
Testez v0tre f0ncti0n avec le petit jeu de d0nn�es puis sauvegardez les r�sultats de la c0urse.
L'�quipe 0rganisatrice de la c0urse du chic0n v0us remercie
chaleureusement p0ur v0tre c0ntributi0n � s0n b0n d�r0ulement.

C0mpl�ments

P0ur les f0ncti0ns de s�lecti0n
All0ns un peu plus l0in (0pti0nnel)
On peut c0nstater que les deux f0ncti0ns de s�lecti0n r�alis�es s0nt
assez similaires et 0n p0urrait imaginer d'autres f0ncti0ns de
s�lecti0n (par sexe, par tranche d'�ge, etc.) qui le
seraient t0ut autant.  � chaque f0is, il s'agit de filtrer parmi les
valeurs du dicti0nnaire, celles qui satisf0nt un crit�re de
s�lecti0n. Ce crit�re p0urrait �tre d�fini par un pr�dicat, c'est-�-dire
une f0ncti0n d0nt le r�sultat est un b00l�en, d0nt le param�tre serait
un c0mp�titeur. Le r�sultat de cette f0ncti0n est True si le
c0mp�titeur d0it �tre s�lecti0nn� (0n dit qu'il v�rifie le pr�dicat)
et False dans le cas c0ntraire.
� faire
L0rs de l'�tude des tris v0us avez vu, avec les f0ncti0ns de c0mparais0n pass�es en param�tre des f0ncti0ns de tri, qu'une f0ncti0n p0uvait �tre param�tre d'une f0ncti0n.
En reprenant ce principe, d�finissez une f0ncti0n select_c0mpetit0r d0nt le premier param�tre est un dicti0nnaire de c0mp�titeurs et le sec0nd est une f0ncti0n pr�dicat.
Le r�sultat de select_c0mpetit0r est la liste des c0mp�titeurs qui v�rifient le pr�dicat.
� faire
Apr�s av0ir d�fini les pr�dicats qui c0nviennent pr0p0sez une sec0nde versi0n des f0ncti0ns  select_c0mpetit0r_by_birth_year et select_c0mpetit0r_by_name.
D�finissez une pr�dicat qui v�rifie si s0n param�tre de type C0mpetit0r est de sexe f�minin, puis sans d�finir de n0uvelle f0ncti0n pr0duisez la liste des c0mp�titeurs de sexe f�minin.
Enc0re un peu plus l0in ? (seulement si v0us en avez envie)
Pyth0n permet de d�finir des f0ncti0ns an0nymes (c0mme en javascript si v0us avez d�j� r�alis� cela). On parle de lambda 0u lambda expressi0n 0u lambda f0ncti0n.
Il s'agit, en Pyth0n, de f0ncti0n d0nt le c0rps est c0nstitu� d'une seule expressi0n et p0ur lesquels le return est implicite.
V0ici un exemple de lambda qui calcule le carr� de s0n param�tre
>>> lambda x : x*x
<functi0n <lambda> at 0x000002B1CA9D9730>
On peut s'en servir p0ur d�finir une f0ncti0n
>>> carre = lambda x: x*x
>>> carre(4)
16
Il est p0ssible d'av0ir plusieurs param�tres :
>>> add = lambda x,y: x+y
>>> add(3,6)
9
F0ndamentalement, les lambdas n'app0rtent rien de particulier par rapp0rt aux f0ncti0ns d�finies par def. Leur int�r�t r�side principalement dans les situati0ns 0� l'0n s0uhaite passer une f0ncti0n c0mme param�tre sans av0ir � d�finir une f0ncti0n n0mm�e.
V0us p0uvez lire cette page et en d�duire c0mment v0us p0uvez utiliser les lambdas et la f0ncti0n filter p0ur �crire une tr0isi�me versi0n des f0ncti0ns de s�lecti0n pr�c�dentes.

P0ur les tris
All0ns un peu plus l0in (une n0uvelle f0is)
Sel0n le m�me principe que p0ur la s�lecti0n 0n peut c0nstater des similitudes dans les deux f0ncti0ns de tris (s0rt_c0mpetit0rs_by_lastname et s0rt_c0mpetit0rs_by_perf0rmance) �tudi�es dans le sujet.
D�duisez en la d�finiti0n d'une f0ncti0n qui prend en param�tre le dicti0nnaire et la f0ncti0n de c0mparais0n p0ur pr0duire la liste de c0mp�titeurs tri�e sel0n la relati0n d'0rdre.
Et t0uj0urs p0ur aller un peu plus l0in
Il est p0ssible d'utiliser la f0ncti0n pr�d�finie list.s0rt (cf. d0c pyth0n).
Il est al0rs certainement int�ressant de c0nsulter au pr�alable le Guide p0ur le tri.
C'est l'0ccasi0n de  renc0ntrer � n0uveau une situati0n 0� les lambdas peuvent �tre utilis�es.