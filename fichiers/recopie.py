def recopie(fichier_entree,fichier_sortie):
    """
    recopie le contenu d'un fichier dans un autre après avoir remplacer les "o" par des "0"
    :param fichier_entree:(str) le nom du fichier source
    :param fichier_sortie:(str) le nom du fichier final
    :return: none
    :effet de bord: modifie le fichier final s'il existe
    """
    with open(fichier_entree,"r") as fin:
        with open(fichier_sortie,"w") as fout:
            for ligne in fin:
                fout.write(ligne.replace("o","0"))