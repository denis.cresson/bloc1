def creer_grille(largeur,hauteur):
    """
    construit une grille vide de la bonne taille
    :param largeur:(int)
    :param hauteur:(int)
    :CU: None
    
    >>> creer_grille(3,2)
    [[0, 0, 0], [0, 0, 0]]
    """
    return [[0 for i in range(largeur)]for j in range(hauteur)]

def hauteur_grille(grille):
    """
    renvoie la hauteur d'une grille
    :param grille:(list)
    :return: (int)
    
    >>>hauteur_grille(creer_grille(3,2))
    2
    """
    return len(grille)

def largeur_grille(grille):
    """
    renvoie la hauteur d'une grille
    :param grille:(list)
    :return: (int)
    
    >>> largeur_grille(creer_grille(3,2))
    3
    """
    return len(grille[0])

def creer_grille_aleatoire(largeur,hauteur,proba):
    """
    construit une grille aleatoire avec une probabilité de presence
    :param largeur:(int)
    :param hauteur:(int)
    :param proba: (float)
    :return:(list)
    :CU: proba entre 0 et 1
    
    >>> creer_grille_aleatoire(3,2,1)
    [[1, 1, 1], [1, 1, 1]]
    >>> creer_grille_aleatoire(3,2,0)
    [[0, 0, 0], [0, 0, 0]]
    >>> creer_grille_aleatoire(3,2,0.5)
    [[1, 0, 0], [0, 0, 1]]
    """
    from random import random
    return [[int(random()<proba) for i in range(largeur)]for j in range(hauteur)]

def voisins_case(grille,abscisse,ordonnée):
    """
    renvoie une liste des valeurs voisines d'une case de grille
    :param grille:(list)
    :param abscisse:(int)
    :param ordonnée:(int)
    :return:(list)
    :CU:None
    
    >>> grille=[[0,1,0],[1,0,0],[1,1,1]]
    >>> voisins_case(grille,0,2)
    [1, 0, 1]
    >>> grille=[[0,1,0],[1,0,0],[1,1,1]]
    >>> voisins_case(grille,1,1)
    [0, 1, 0, 1, 0, 1, 1, 1]
    >>> voisins_case(grille,2,2)
    [0, 0, 1]
    >>> voisins_case(grille,0,2)
    [1, 0, 1]
    """
    retourne=[]
    for j in range(max(0,ordonnée-1),min(hauteur_grille(grille),ordonnée+2)):
        for i in range(max(0,abscisse-1),min(largeur_grille(grille),abscisse+2)):
            if (i,j)!=(abscisse,ordonnée):
                retourne.append(grille[j][i])
    return retourne

def nb_cellules_voisins(grille,abscisse,ordonnée):
    """
    retourne le nombre de voisins de la grille
    :param grille:(list)
    :param largeur:(int)
    :param hauteur:(int)
    :return:(int)
    :CU:None
    
    >>> grille=[[0,1,0],[1,0,0],[1,1,1]]
    >>> nb_cellules_voisins(grille,1,1)
    5
    >>> nb_cellules_voisins(grille,2,2)
    1
    """
    retourne=0
    for i in voisins_case(grille,abscisse,ordonnée):
        retourne+=i
    return retourne
    
def afficher_grille(grille):
    """
    affiche la grille en utilisant des O et des _
    :param grille:(list)
    :CU:None
    
    >>> grille=[[0,1,0],[1,0,0],[1,1,1]]
    >>> afficher_grille(grille)
    _ O _ 
    O _ _ 
    O O O
    >>> afficher_grille(creer_grille(3,2))
    _ _ _ 
    _ _ _ 
    """
    for ligne in grille:
        for val in ligne:
            print("_" if val==0 else "O" ,end=" ")
        print("")

def generation_suivante(grille):
    """
    genere la grille suivante (après une évolution)
    :param grille:(list)
    :return:(list)
    :CU: None
    
    >>> grille=[[0,1,0],[1,0,0],[1,1,1]]
    >>> generation_suivante(grille)
    [[0, 0, 0], [1, 0, 0], [1, 1, 0]]
    >>> generation_suivante([[0, 0, 0], [1, 0, 1], [1, 1, 0]])
    [[0, 0, 0], [1, 0, 0], [1, 1, 0]]
    >>> generation_suivante([[0, 0, 0], [1, 0, 0], [1, 1, 0]])
    [[0, 0, 0], [1, 1, 0], [1, 1, 0]]
    """
    from copy import copy
    retourne=creer_grille(largeur_grille(grille),hauteur_grille(grille))
    for i in range(largeur_grille(grille)):
        for j in range(hauteur_grille(grille)):
            if (grille[j][i]==0 and nb_cellules_voisins(grille,i,j)==3):
                retourne[j][i]=1
            elif (grille[j][i]==1 and (nb_cellules_voisins(grille,i,j)<2 or nb_cellules_voisins(grille,i,j)>3)):
                retourne[j][i]=0
            else:
                retourne[j][i]=grille[j][i]
    return retourne
        
def evolution_n_generations(grille,n):
    """
    genere les n grilles suivante (après n évolutions)
    :param grille:(list)
    :param n:(int)
    :CU: None
    
    :exemple:
    
    oscilateur
    
    >>> grille=[[0, 0, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1], [0, 1, 0, 0]]
    >>> evolution_n_generations(grille,10)
    _ _ _ _ 
    _ O O O 
    O O O _ 
    _ _ _ _ 

    _ _ O _ 
    O _ _ O 
    O _ _ O 
    _ O _ _ 

    _ _ _ _ 
    _ O O O 
    O O O _ 
    _ _ _ _ 

    _ _ O _ 
    O _ _ O 
    O _ _ O 
_ O _ _ 

    """
    from time import sleep
    from copy import copy
    GRILLE=copy(grille)
    for i in range(n):
        GRILLE=generation_suivante(GRILLE)
        afficher_grille(GRILLE)
        sleep(1)
        print()
             