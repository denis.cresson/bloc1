def somme(a,b):
    """
    retourne la somme de deux nombres en paramètres
    :param a:(float)
    :param b:(float)
    :return:(float)
    """
    if a==0:
        return b
    else:
        return somme(a-1,b+1)
    
def produit(a,b):
    """
    retourne le produit de deux nombres en paramètres
    :param a:(float)
    :param b:(float)
    :return:(float)
    """
    if a==0:
        return 0
    else:
        return b+ produit(a-1,b)
    
def puissance(a,n):
    """
    calcule la nième puissance d'un nombre en paramètre
    :param a:(float)
    :param n:(int)
    :return:(float)
    """
    if n==0:
        return 1
    else :
        return a*puissance(a,n-1)
    
def pgcd(a,b):
    """
    calcule le pgcd de deux nombres en paramètres
    :param a:(int)
    :param b:(int)
    :return:(bool)
    """
    if a%b==0:
            return b
    else :
        return pgcd(b,a%b)
    
def palind(chaine):
    """prédicat qui teste sdi une chaine en paramètre est un palindrome
    :param chaine:(str)
    :return:(bool)
    """
    if len(chaine)<2:
        return True
    else :
        if chaine[0]==chaine[-1]:
            return palind(chaine[1:-1])
        else :
            return False

def romain_to_arabe(chaine):
    """retourne la valeur entière dun nombre ecrit en chiffres romains
    :param chaine:(str)
    :return:(int)
    """
    VALEUR_ROMAIN = { 'M' : 1000, 'D' : 500, 'C' : 100, 'L' : 50, 'X' : 10, 'V' : 5, 'I' : 1}
    if len(chaine)==1:
        return VALEUR_ROMAIN[chaine[0]]
    else :
        if VALEUR_ROMAIN[chaine[1]]<=VALEUR_ROMAIN[chaine[0]]:
            return VALEUR_ROMAIN[chaine[0]]+romain_to_arabe(chaine[1:])
        else :
            return -VALEUR_ROMAIN[chaine[0]]+romain_to_arabe(chaine[1:])
        
def permute (chaine):
    """
    fonction qui construit la liste des permutations d'une chaine en paramètre
    :param chaine:(str)
    :return:(list)
    """
    if len(chaine)==1:
        return [chaine]
    else :
        result=[]
        for i in range(len(chaine)):
            #un charactère est choisit
            for p in permute(chaine[:i]+chaine[i+1:]):
                #une permutation de la chaine des caractères restant est choisit
                result.append(chaine[i]+p)
    return result

def somme_liste(liste):
    """ fonction qui retourne la somme d'une liste
    :param liste:(list)
    :return:(float ou int ou str)
    :CU: fournir une liste d'objet qui sont admis par "+"
    """
    if liste==[]:
        return 0
    else:
        return liste[0]+somme_liste(liste[1:])
class listeVide(Exception):
    pass

def last(liste):
    """
    fonction qui retourne la longueur d'une liste
    :param liste:(list)
    :return:(int)
    """
    if liste==[]:
        raise listeVide
    if liste[1:]==[]:
        return liste[0]
    else:
        return last(liste[1:])
    
def concat(liste1,liste2):
    """concatène deux listes en paramètre et retourne la liste obtenue
    :param liste1:(list)
    :param liste2:(list)
    :return:(liste)
    """
    if liste1==[]:
        return liste2
    else:
        return [liste1[0]]+concat(liste1[1:],liste2)

def carre(x):
    return x**2

def map(f,liste):
    """ Appliquer une fonction f à tous les éléments d'une liste l,
    c'est construire une liste contenant les images f(x) de tous les éléments x de l.
    :param f: (function)
    :param liste:(list)
    :return:(list)
    """
    if liste==[]:
        return []
    else:
        return [f(liste[0])]+map(f,liste[1:])

def shuffle(liste1,liste2):
    """
    fonction paramétrée par deux listes l1 et l2 qui renvoie une liste dont les éléments sont ceux
    de ces deux listes dans un ordre alterné, tant que c'est possible.
    Si l'une des listes est plus courte que l'autre, on termine avec les éléments non utilisés de la plus longue liste.
    :param Liste1:(list)
    :param Liste2:(list)
    :return:(list)
    """
    if liste1==[]:
        return liste2
    else:
        return [liste1[0]]+shuffle(liste2,liste1[1:])

## hanoi
    
def deplace(L=[5,4,3,2,1],deb=1,fin=3):
    """
    fonction de résolution du problème des tours de hanoi en relation avec la fonction bouge
    :param L:(list) la pile à déplacer
    :param deb:(int) la position de départ
    :param fin:(int) la position de fin
    """
    if len(L)==1:
        bouge(L[0],deb,fin)
    else:
        positions=[1,2,3]
        positions.remove(deb)
        positions.remove(fin)
        autre=positions[0]
        deplace(L[1:],deb,autre)
        bouge(L[0],deb,fin)
        deplace(L[1:],autre,fin)

def bouge(pion,deb,fin):
    """
    ecrit les déplacements le déplacement prévu
    :param pion:(int) la taille du pion
    :param deb:(int) la colonne de départ
    :param fin:(int) la colonne de fin
    :effet de bord: affiche sur la sortie standart
    """
    print(f"bouger le pion de taille {pion} de la colonne {deb} à la colonne {fin}")
    
def tri_insertion(Liste):
    """
    trie une liste en paramètre par insertion et retourne le résultat (utilise place)
    :param Liste:(liste)
    :return:(list)
    >>> tri_insertion([1,5,2,7,3,4,9,8,5])
    [1, 2, 3, 4, 5, 5, 7, 8, 9]
    """
    if len(Liste)==1:
        return(Liste)
    else:
        return place(Liste[0],tri_insertion(Liste[1:]))
def place(valeur,Liste):
    """
    place une valeur à sa place dans une liste triée par ordre croissant et la retourne
    :param valeur:(float)
    :param Liste:(list)
    :return:(list)
    >>> place(3,[1,2,7,8])
    [1, 2, 3, 7, 8]
    >>> place(0,[1,2,7,8])
    [0, 1, 2, 7, 8]
    >>> place(10,[1,2,7,8])
    [1, 2, 7, 8, 10]
    """
    if Liste==[] or valeur<Liste[0]:
        return [valeur]+Liste
    else:
        return [Liste[0]]+place(valeur,Liste[1:])

def permutation(Liste):
    """
    produit la liste de toutes les permutations des éléments d'une liste
    :param Liste:(list)
    :return:(Liste) de listes
    >>> permutation(["Belle Marquise","vos beaux yeux","me font mourir","d'amour"])
    [['Belle Marquise', 'vos beaux yeux', 'me font mourir', "d'amour"],['Belle Marquise', 'vos beaux yeux', "d'amour", 'me font mourir'],
    ['Belle Marquise', 'me font mourir', 'vos beaux yeux', "d'amour"],['Belle Marquise', 'me font mourir', "d'amour", 'vos beaux yeux'],
    ['Belle Marquise', "d'amour", 'vos beaux yeux', 'me font mourir'],['Belle Marquise', "d'amour", 'me font mourir', 'vos beaux yeux'],
    ['vos beaux yeux', 'Belle Marquise', 'me font mourir', "d'amour"],['vos beaux yeux', 'Belle Marquise', "d'amour", 'me font mourir'],
    ['vos beaux yeux', 'me font mourir', 'Belle Marquise', "d'amour"],['vos beaux yeux', 'me font mourir', "d'amour", 'Belle Marquise'],
    ['vos beaux yeux', "d'amour", 'Belle Marquise', 'me font mourir'],['vos beaux yeux', "d'amour", 'me font mourir', 'Belle Marquise'],
    ['me font mourir', 'Belle Marquise', 'vos beaux yeux', "d'amour"],['me font mourir', 'Belle Marquise', "d'amour", 'vos beaux yeux'],
    ['me font mourir', 'vos beaux yeux', 'Belle Marquise', "d'amour"],['me font mourir', 'vos beaux yeux', "d'amour", 'Belle Marquise'],
    ['me font mourir', "d'amour", 'Belle Marquise', 'vos beaux yeux'],['me font mourir', "d'amour", 'vos beaux yeux', 'Belle Marquise'],
    ["d'amour", 'Belle Marquise', 'vos beaux yeux', 'me font mourir'],["d'amour", 'Belle Marquise', 'me font mourir', 'vos beaux yeux'],
    ["d'amour", 'vos beaux yeux', 'Belle Marquise', 'me font mourir'],["d'amour", 'vos beaux yeux', 'me font mourir', 'Belle Marquise'],
    ["d'amour", 'me font mourir', 'Belle Marquise', 'vos beaux yeux'],["d'amour", 'me font mourir', 'vos beaux yeux', 'Belle Marquise']]
    """
    
    if len(Liste)==1:
        return [Liste]
    else:
        result=[]
        for i in range(len(Liste)):
            for perm in permutation(Liste[:i]+Liste[i+1:]):
                result.append([Liste[i]]+perm)
        return result

def rencontre(Liste):
    """
    retourne la liste des matchs possibles dans un championnat dont la liste des joueurs est en paramètre
    :param Liste:(liste) de nombres (ordonnés)
    :return:(list) de tuples
    >>> championnat([1,2,3,4,5,6])
    [(1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (2, 2),(2, 3), (2, 4),
    (2, 5), (2, 6), (3, 3), (3, 4), (3, 5), (3, 6), (4, 4), (4, 5), (4, 6), (5, 6)]
    """
    if len (Liste)==2:
        return [tuple(Liste)]
    else:
        return [(Liste[0],elt) for elt in Liste]+ rencontre(Liste[1:])

DICO = ["aime", "auge", "baie", "brie", "bris", "bure", "cage", "cale", "came", "cape",
        "cime", "cire", "cris", "cure", "dame", "dime", "dire", "ducs", "dues", "duos",
        "dure", "durs", "fart", "fors", "gage", "gaie", "gais", "gale", "gare", "gars",
        "gris", "haie", "hale", "hors", "hure", "iris", "juge", "jure", "kart", "laie",
        "lame", "lime", "lire", "loge", "luge", "mage", "maie", "male", "mare", "mari",
        "mars", "mere", "mers", "mime", "mire", "mors", "muet", "mure", "murs", "nage",
        "orge", "ours", "page", "paie", "pale", "pame", "pane", "pape", "pare", "pari",
        "part", "paru", "pere", "pers", "pipe", "pire", "pore", "prie", "pris", "pues",
        "purs", "rage", "raie", "rale", "rame", "rape", "rare", "rime", "rire", "sage",
        "saie", "sale", "sape", "sari", "scie", "sure", "taie", "tale", "tape", "tare",
        "tari", "tige", "toge", "tore", "tors", "tort", "trie", "tris", "troc", "truc"]

def solve(mot1,mot2,visites=None):
    """
    retourne la liste de mots d'une liste DICO permettant de passer du premier mot en paramètre au second en ne changeant qu'une lettre
    à la fois (sans tenir compte de l'ordre) quand elle existe et `None` dans le cas contraire.
    :param mot1:(str)
    :param mot2:(str)
    :return:(list) de str ou none
    >>> print(solve('ours', 'cage'))
    ['ours', 'duos', 'ducs', 'dues', 'dure', 'bure', 'brie', 'baie', 'aime', 'came', 'cage']
    >>> print(solve('ours', 'truc'))
    ['ours', 'duos', 'ducs', 'dues', 'dure', 'bure', 'brie', 'baie', 'aime', 'came', 'cage', 'auge', 'gaie', 'gais', 'gars',
    'gare', 'gale', 'cale', 'cape', 'page', 'mage', 'dame', 'dime', 'cime', 'cire', 'cris', 'bris', 'gris', 'pris', 'pari',
    'mari', 'maie', 'haie', 'hale', 'laie', 'lame', 'lime', 'lire', 'dire', 'mire', 'mare', 'male', 'pale', 'paie', 'pame',
    'pane', 'nage', 'rage', 'orge', 'loge', 'luge', 'juge', 'jure', 'cure', 'truc']
    >>> solve('ours', 'orme')
    ['ours', 'duos', 'ducs', 'dues', 'dure', 'bure', 'brie', 'baie', 'aime', 'came', 'cage', 'auge', 'gaie', 'gais', 'gars', 'gare',
    'gale', 'cale', 'cape', 'page', 'mage', 'dame', 'dime', 'cime', 'cire', 'cris', 'bris', 'gris', 'pris', 'pari', 'mari', 'maie',
    'haie', 'hale', 'laie', 'lame', 'lime', 'lire', 'dire', 'mire', 'orme']
    >>> solve('mars', 'gage')
    False
    """
    if visites==None:
        visites=[]
    visitables=[mot for mot in DICO if (voisin(mot,mot1)) and (mot not in visites) and (mot!=mot1)]
    if len(visites)==len(DICO):
        return False
    if voisin(mot1,mot2):
        return [mot1,mot2]
    for mot in visitables:
        if voisin(mot,mot1):
            visites.append(mot1)
            try:
                return [mot1]+solve(mot,mot2,visites)
            except:
                return solve(visites[-2],mot2,visites)
    visites=visites[:-1]
    return False       
    
def voisin(mot1,mot2):
    """ verifie si deux mots sont voisins (une seule lettre d'ecart à l'ordre pret)
    :param mot1:(str)
    :param mot2:(str)
    :return:(bool)
    >>> voisin("azer","ytza")
    False
    >>> voisin("azer","etza")
    True
    """
    mot2bis=mot2
    for l in mot1:
        mot2=mot2.replace(l,"")
    for l in mot2bis:
        mot1=mot1.replace(l,"")
    return len(mot2)==1 and len(mot1)==1
import turtle
def segment(l,n):
    """
    dessine un "segment" de niveau n
    :param l:(float)
    :param n:(int)
    :effet de bord: dessine avec turtle
    """
    if n==1:
        turtle.forward(l)
    else:
       segment(l/3,n-1)
       turtle.left(60)
       segment(l/3,n-1)
       turtle.right(120)
       segment(l/3,n-1)
       turtle.left(60)
       segment(l/3,n-1)

def flocon(l,n):
    """
    dessine un flocon de taille l d'ordre n
    :param l:(float)
    :param n:(int)
    :effet de bord: dessine avec turtle
    >>> flocon(100,4)
    """
    segment(l,n)
    turtle.right(120)
    segment(l,n)
    turtle.right(120)
    segment(l,n)